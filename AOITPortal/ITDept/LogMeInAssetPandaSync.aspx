﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="LogMeInAssetPandaSync.aspx.vb" Inherits="AOITPortal.LogMeInAssetPandaSync" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="nav-justified">
                <tr>
                    <td style="height: 20px">&nbsp;</td>
                    <td style="height: 20px">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center" style="height: 20px">
                        <strong>LogMeIn to Assetpanda Integration Tool</strong></td>
                </tr>
                <tr>
                    <td class="text-center" style="height: 20px">1- Import Data From LogMeIn</td>
                    <td class="text-center" style="height: 20px">
                        <asp:Button ID="Button1" runat="server" Text="Import Data From LogMeIn" Width="204px" />
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="height: 20px">&nbsp;</td>
                    <td class="text-center" style="height: 20px">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center" style="height: 20px">2-&nbsp; Send data to Asset Panda</td>
                    <td class="text-center" style="height: 20px">
                        <asp:Button ID="Button2" runat="server" Text="Send To AssetPanda" />
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="height: 20px">
                        &nbsp;</td>
                    <td class="text-center" style="height: 20px">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center" style="height: 20px">Items Number</td>
                    <td class="text-center" style="height: 20px">
                        <asp:Label ID="Label1" runat="server" Text="0"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <asp:Image ID="Image1" runat="server" Height="52px" ImageUrl="~/Image/SkinnySeveralAsianlion.gif" Width="60px" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>

                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="AssetID" DataSourceID="SqlDataSource1" ForeColor="Black" GridLines="Horizontal" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="AssetID" HeaderText="Asset ID" ReadOnly="True" SortExpression="AssetID" />
                                <asp:BoundField DataField="AssetName" HeaderText="Asset Name" SortExpression="AssetName" />
                                <asp:BoundField DataField="Manufact" HeaderText="Manufacturer" SortExpression="Manufact" />
                                <asp:BoundField DataField="Model" HeaderText="Model" SortExpression="Model" />
                                <asp:BoundField DataField="ServiceTag" HeaderText="Service Tag" SortExpression="ServiceTag" />
                                <asp:BoundField DataField="LastExport" HeaderText="Last Export" SortExpression="LastExport" />
                                <asp:BoundField DataField="ExportUser" HeaderText="Export User" SortExpression="ExportUser" />
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#242121" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:AOWebPortalConnectionString %>" SelectCommand="SELECT * FROM [View_AssetMaster]"></asp:SqlDataSource>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
