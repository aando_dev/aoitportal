﻿Imports System.Net
Imports Newtonsoft.Json.Linq
Imports System.IO
Imports RestSharp



Public Class LogMeInAssetPandaSync
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ScreenID As Integer = 3
        Try
            Dim dapUA As New AOWebPortaDSTableAdapters.UserAccessTableAdapter
            Dim ds As New AOWebPortaDS.UserAccessDataTable
            dapUA.FillByUserAndScreen(ds, User.Identity.Name, ScreenID)
            If ds.Rows.Count = 0 Then
                Response.Redirect("../Error.aspx")
            Else
                Dim dapLog As New AOWebPortaDSTableAdapters.LoginLogsTableAdapter
                dapLog.InsertLogin(User.Identity.Name, ScreenID)
            End If
        Catch ex As Exception
            Response.Redirect("../Error.aspx")
        End Try
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim client = New RestClient("https://secure.logmein.com/public-api/v2/hosts")
        Dim request = New RestRequest(Method.[GET])
        request.AddHeader("Postman-Token", "3887d4a3-4806-4b03-9538-542d5f2cf4c7")
        request.AddHeader("Cache-Control", "no-cache")
        request.AddHeader("Authorization", "Basic MTAxMTE5OTU0MTowMV9ibEJWUkdiRlJNQWw3Q2ZESjVnMGdYTUh4c3lLV1h5R3QwdmVCQzRhREl5aDFBTFZQMWNOcDdkcmR3Y1RDSjRPNzk5ekVhZlhYa0FoT1ZLWnRpM1puVXRkbkN0UmprR1Z4N1NvdTBydWg1a0pFZGUzS1dLMXV4QlRyM2pwRA==")
        Dim response As IRestResponse = client.Execute(request)
        Dim jsonData As String = response.Content
        Dim allData As JObject = JObject.Parse(jsonData)
        Dim minecraftDataList As New List(Of LogmeInHost)
        Dim dap As New AOWebPortaDSTableAdapters.Ast_AssetMasterTableAdapter
        dap.Truncate()
        Dim HosIds As String = "["
        Try
            For Each token As JToken In allData("hosts").Children
                Try
                    dap.InsertNew(token.Value(Of Integer)("id"), token.Value(Of String)("description"), token.Value(Of String)("isHostOnline"))
                Catch ex As Exception
                End Try
                HosIds = HosIds + token.Value(Of String)("id") + ","
                'minecraftDataList.Add(New LogmeInHost With {.description = token.Value(Of String)("description"), .id = token.Value(Of String)("id"), .isHostOnline = token.Value(Of String)("isHostOnline")})
            Next
            CreateHostReport(HosIds + "],")
        Catch ex As Exception
            MsgBox("Server Busy, Please wait Few minutes to Start new Import!")
        End Try

        'GridView1.DataSource = minecraftDataList
        'GridView1.DataBind()
    End Sub
    Private Sub CreateHostReport(ByVal hostid As String)
        Dim client = New RestClient("https://secure.logmein.com/public-api/v1/inventory/hardware/reports")
        Dim request = New RestRequest(Method.POST)
        request.AddHeader("Postman-Token", "e38756e4-4fde-4e5d-978e-8548e507a433")
        request.AddHeader("Cache-Control", "no-cache")
        request.AddHeader("Authorization", "Basic MTAxMTE5OTU0MTowMV9ibEJWUkdiRlJNQWw3Q2ZESjVnMGdYTUh4c3lLV1h5R3QwdmVCQzRhREl5aDFBTFZQMWNOcDdkcmR3Y1RDSjRPNzk5ekVhZlhYa0FoT1ZLWnRpM1puVXRkbkN0UmprR1Z4N1NvdTBydWg1a0pFZGUzS1dLMXV4QlRyM2pwRA==")
        request.AddHeader("Content-Type", "application/json")
        request.AddParameter("undefined", "{" & vbCrLf & "  ""hostIds"":" + hostid + "" & vbCrLf & "  ""fields"": [" & vbCrLf & "    ""HardwareAssetTag""," & vbCrLf & "    ""HardwareManufacturer""," & vbCrLf & "    ""HardwareModel""," & vbCrLf & "    ""ServiceTag""]" & vbCrLf & "}", ParameterType.RequestBody)
        Dim response As IRestResponse = client.Execute(request)
        Dim jsonData As String = response.Content
        Dim allData As JObject = JObject.Parse(jsonData)
        Dim reporttoken As String = allData.Value(Of String)("token")
        ReadHostReport(reporttoken)
    End Sub
    Private Sub ReadHostReport(ByVal reporttoken As String)
        Dim client = New RestClient("https://secure.logmein.com/public-api/v1/inventory/hardware/reports/" + reporttoken + "")
        Dim request = New RestRequest(Method.[GET])
        request.AddHeader("Postman-Token", "ba816102-8ca2-4042-a922-265f77920f90")
        request.AddHeader("Cache-Control", "no-cache")
        request.AddHeader("Authorization", "Basic MTAxMTE5OTU0MTowMV9ibEJWUkdiRlJNQWw3Q2ZESjVnMGdYTUh4c3lLV1h5R3QwdmVCQzRhREl5aDFBTFZQMWNOcDdkcmR3Y1RDSjRPNzk5ekVhZlhYa0FoT1ZLWnRpM1puVXRkbkN0UmprR1Z4N1NvdTBydWg1a0pFZGUzS1dLMXV4QlRyM2pwRA==")
        Dim response As IRestResponse = client.Execute(request)
        Dim jsonData As String = response.Content
        Dim allData As JObject = JObject.Parse(jsonData)
        ' Dim minecraftDataList As New List(Of LMIHostReport)
        Dim hosidval As Integer
        Dim sertagval As String
        Dim manufactval As String
        Dim modelval As String
        Dim assetTagval As String
        Dim hardware As JToken
        Dim dap As New AOWebPortaDSTableAdapters.Ast_AssetDetailsTableAdapter
        dap.Truncate()

        For Each hosttoken As JToken In allData("hosts").Children
            For Each hostid As JToken In hosttoken.Children
                hosidval = hostid.Value(Of Integer)("hostId")
                sertagval = hostid.Value(Of String)("serviceTag")
                hardware = hostid("hardwareInfo")
                manufactval = hardware.Value(Of String)("manufacturer")
                modelval = hardware.Value(Of String)("model")
                '  assetTagval = hardware.Value(Of String)("assetTag")
            Next
            Try
                dap.InsertAssetDet(hosidval, manufactval, modelval, sertagval)
            Catch ex As Exception
            End Try
            '     minecraftDataList.Add(New LMIHostReport With {.hostId = hosidval, .serviceTag = sertagval, .manufacturer = manufactval, .model = modelval, .assetTag = assetTagval})
        Next
        ' GridView1.DataSource = minecraftDataList
        GridView1.DataBind()
        Label1.Text = GridView1.Rows.Count
    End Sub
    Private Sub ExportToAssetPanda(ByVal sertag As String, ByVal manufacturer As String, ByVal model As String, ByVal assetname As String)
        Dim client = New RestClient("https://api.assetpanda.com:443/v2/entities/95047/objects")
        Dim request = New RestRequest(Method.POST)
        request.AddHeader("Postman-Token", "ebc9837f-fba7-45fc-b4ac-dbfc836d2e78")
        request.AddHeader("Cache-Control", "no-cache")
        request.AddHeader("Authorization", "Bearer 5739318390a2dd85666082463aa6d1039a3ca9ae00660bb2aa16a6a4361644e0")
        request.AddHeader("Content-Type", "application/json")
        request.AddParameter("undefined", "{""field_10"": """ + sertag + """,""field_4"": """ + assetname + """,""field_6"": """ + manufacturer + """,""field_8"": """ + model + """ }", ParameterType.RequestBody)
        Dim response As IRestResponse = client.Execute(request)
    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        '  Dim ExportedItems As Integer = 0
        'Dim Notexported As Integer = 0
        Dim dap As New AOWebPortaDSTableAdapters.Ast_AsstTransTableAdapter
        If GridView1.Rows.Count > 0 Then
            For Each ro As GridViewRow In GridView1.Rows
                Try
                    'ExportToAssetPanda(ro.Cells(4).Text, ro.Cells(2).Text, ro.Cells(3).Text, ro.Cells(1).Text)
                    ' ExportedItems += 1
                Catch ex As Exception
                    '  Notexported += 1
                End Try
                Try
                    dap.InsertExport(ro.Cells(0).Text, User.Identity.Name)
                Catch ex As Exception

                End Try
            Next
            GridView1.DataBind()
            Label1.Text = GridView1.Rows.Count
        End If
        ' ExportToAssetPanda("hazemtestWeb1SErTag", "AppelTest", "ModelTest", "assetnameTest")
    End Sub
End Class