﻿


Class TransToWH
    Inherits System.Web.UI.Page
    Dim SBy As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click


        SBy = DropDownList2.SelectedValue
        Try
            Select Case DropDownList2.SelectedValue
                Case 0
                    TransToWHDS.SelectCommand = "SELECT Org_Store, UPC, QOH, Pending_Qnty, Style, Color_Code, Color_Name, Size, Available_Qnty FROM View_ForTransToWH WHERE UPC =" + TXTSearch.Text

                Case 1
                    TransToWHDS.SelectCommand = "SELECT Org_Store, UPC, QOH, Pending_Qnty, Style, Color_Code, Color_Name, Size, Available_Qnty FROM View_ForTransToWH WHERE Style ='" + TXTSearch.Text + "'"
                Case 2
                    TransToWHDS.SelectCommand = "SELECT Org_Store, UPC, QOH, Pending_Qnty, Style, Color_Code, Color_Name, Size, Available_Qnty FROM View_ForTransToWH WHERE Color_Code ='" + TXTSearch.Text + "'"

            End Select
            TransToWHDS.DataBind()

        Catch ex As Exception

        End Try


    End Sub




    Private Sub GridView1_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles GridView1.RowCancelingEdit
        Try
            Select Case DropDownList2.SelectedValue
                Case 0
                    TransToWHDS.SelectCommand = "SELECT Org_Store, UPC, QOH, Pending_Qnty, Style, Color_Code, Color_Name, Size, Available_Qnty FROM View_ForTransToWH WHERE UPC =" + TXTSearch.Text

                Case 1
                    TransToWHDS.SelectCommand = "SELECT Org_Store, UPC, QOH, Pending_Qnty, Style, Color_Code, Color_Name, Size, Available_Qnty FROM View_ForTransToWH WHERE Style ='" + TXTSearch.Text + "'"
                Case 2
                    TransToWHDS.SelectCommand = "SELECT Org_Store, UPC, QOH, Pending_Qnty, Style, Color_Code, Color_Name, Size, Available_Qnty FROM View_ForTransToWH WHERE Color_Code ='" + TXTSearch.Text + "'"

            End Select
            TransToWHDS.DataBind()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub GridView1_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles GridView1.RowUpdating

        Label1.Visible = False
        Dim row As GridViewRow = GridView1.Rows(e.RowIndex)
        Dim storfrom As Integer = Convert.ToInt32(GridView1.Rows(e.RowIndex).Cells(0).Text)
        Dim UPC As String = GridView1.Rows(e.RowIndex).Cells(1).Text
        Dim Qntyold As Integer = Convert.ToInt32(row.Cells(4).Text)
        Dim Qnty As String = TryCast(row.Cells(9).Controls(0), TextBox).Text
        Try
            If CInt(Qnty) <= Qntyold Then
                Try
                    TransToWHDS.InsertParameters(0).DefaultValue = UPC
                    TransToWHDS.InsertParameters(1).DefaultValue = Qnty
                    TransToWHDS.InsertParameters(5).DefaultValue = User.Identity.Name
                    TransToWHDS.InsertParameters(2).DefaultValue = storfrom
                    TransToWHDS.InsertParameters(4).DefaultValue = Left(Now.ToShortTimeString, 5)
                    TransToWHDS.Insert()

                    Try
                        Select Case DropDownList2.SelectedValue
                            Case 0
                                TransToWHDS.SelectCommand = "SELECT Org_Store, UPC, QOH, Pending_Qnty, Style, Color_Code, Color_Name, Size, Available_Qnty FROM View_ForTransToWH WHERE UPC =" + TXTSearch.Text

                            Case 1
                                TransToWHDS.SelectCommand = "SELECT Org_Store, UPC, QOH, Pending_Qnty, Style, Color_Code, Color_Name, Size, Available_Qnty FROM View_ForTransToWH WHERE Style ='" + TXTSearch.Text + "'"
                            Case 2
                                TransToWHDS.SelectCommand = "SELECT Org_Store, UPC, QOH, Pending_Qnty, Style, Color_Code, Color_Name, Size, Available_Qnty FROM View_ForTransToWH WHERE Color_Code ='" + TXTSearch.Text + "'"

                        End Select
                        TransToWHDS.DataBind()

                    Catch ex As Exception

                    End Try
                Catch ex As Exception
                    Label1.Visible = True
                End Try
            Else
                Label1.Visible = True

            End If
        Catch ex As Exception
            Label1.Visible = True
            Exit Sub
        End Try
    End Sub

    Private Sub GridView1_RowUpdated(sender As Object, e As GridViewUpdatedEventArgs) Handles GridView1.RowUpdated
        e.KeepInEditMode = False

    End Sub
End Class