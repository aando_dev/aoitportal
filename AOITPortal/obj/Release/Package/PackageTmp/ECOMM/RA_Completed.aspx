﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="RA_Completed.aspx.vb" Inherits="AOITPortal.RA_Completed" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table class="nav-justified">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="text-center" colspan="2"><strong>
                <asp:Label ID="Label1" runat="server" style="color: #000066; font-size: xx-large"></asp:Label>
                </strong></td>
        </tr>
        <tr>
            <td class="text-center" colspan="2">
                        <asp:Button ID="BTN_Process" runat="server" BackColor="#4D4F6F" BorderStyle="Solid" Font-Size="Small" ForeColor="White" Height="40px" Text="Click here to go back to RA Form" Width="272px" />
                    </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>
