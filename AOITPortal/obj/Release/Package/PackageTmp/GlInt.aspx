﻿<%@ Page Language="vb"  MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GlInt.aspx.vb" Inherits="AOITPortal.GlInt" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="nav-justified">
                <tr>
                    <td>&nbsp;</td>
                    <td style="width: 674px">&nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 20px">Month</td>
                    <td style="height: 20px; width: 674px;">
                        
                        <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="MonthDS" DataTextField="MonthName" DataValueField="MonthName" Width="200px">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="MonthDS" runat="server" ConnectionString="<%$ ConnectionStrings:KWIConnectionString %>" SelectCommand="SELECT * FROM [Months]"></asp:SqlDataSource>
                        
                    </td>
                </tr>
                <tr>
                    <td style="height: 19px"></td>
                    <td style="height: 19px; width: 674px;">
                        </td>
                </tr>
                <tr>
                    <td style="height: 19px">Date From </td>
                    <td style="height: 19px; width: 674px;">
                        <asp:TextBox ID="TXTDateFrom" runat="server" Width="167px" Height="21px">Click here to select date</asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="TXTDateFrom_CalendarExtender" runat="server" BehaviorID="TXTDateFrom_CalendarExtender"  Format="yyyy-MM-dd" TargetControlID="TXTDateFrom" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TXTDateFrom" ErrorMessage="Please enter date from" ForeColor="#CC3300"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TXTDateFrom" ErrorMessage="Incorrect Date Format" ForeColor="Red" ValidationExpression="\d{4}-\d{2}-\d{2}"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px"></td>
                    <td style="height: 20px; width: 674px;"></td>
                </tr>
                <tr>
                    <td style="height: 20px">Date To </td>
                    <td style="height: 20px; width: 674px;">
                        <asp:TextBox ID="TXTDateTo" runat="server" Width="168px" Height="22px">Click here to select date</asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="TXTDateTo_CalendarExtender" runat="server" BehaviorID="TXTDateTo_CalendarExtender"  Format="yyyy-MM-dd" TargetControlID="TXTDateTo" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TXTDateTo" ErrorMessage="Please enter date to" ForeColor="#CC3300"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TXTDateTo" ErrorMessage="Incorrect Date Format" ForeColor="Red" ValidationExpression="\d{4}-\d{2}-\d{2}"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px">&nbsp;</td>
                    <td style="height: 20px; width: 674px;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 20px">Prepared By</td>
                    <td style="height: 20px; width: 674px;">
                        <asp:TextBox ID="TXTPrepBy" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px">&nbsp;</td>
                    <td style="height: 20px; width: 674px;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 20px">Batch </td>
                    <td style="height: 20px; width: 674px;">
                        <asp:TextBox ID="TXTBatch" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px">&nbsp;</td>
                    <td style="height: 20px; width: 674px;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 20px">
                        <br />
                        <br />
                    </td>
                    <td style="height: 20px; width: 674px;">
                        <asp:Button ID="Button1" runat="server" BackColor="#3366CC" BorderColor="#003399" BorderStyle="Solid" Font-Bold="True" ForeColor="White" Height="36px" Text="Load Sales and Returns &gt;&gt;" Width="232px" />
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="Button2" runat="server" BackColor="#3366CC" BorderColor="#003399" BorderStyle="Solid" Font-Bold="True" ForeColor="White" Height="36px" Text="Load COGS &gt;&gt;" Width="141px" />
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px" colspan="2">
                        <asp:UpdateProgress ID="UpdateProgress2" runat="server">
                            <ProgressTemplate>
                                <asp:Image ID="Image1" runat="server" Height="52px" ImageUrl="~/Image/SkinnySeveralAsianlion.gif" Width="56px" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 20px">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 20px">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 20px">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" ShowFooter="True">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            <Columns>
                                <asp:BoundField HeaderText="Company" NullDisplayText="D000" />
                                <asp:BoundField DataField="AccountNumber" HeaderText="Account Number" SortExpression="AccountNumber" />
                                <asp:BoundField DataField="SubAccount" HeaderText="Sub Account" SortExpression="SubAccount" />
                                <asp:BoundField DataField="Referance" HeaderText="Ref No." SortExpression="Referance" />
                                <asp:BoundField DataField="Debit" HeaderText="Debit" SortExpression="Debit" />
                                <asp:BoundField DataField="Credit" HeaderText="Credit" SortExpression="Credit" />
                                <asp:BoundField DataField="Descrp" HeaderText="Description" SortExpression="Descrp" />
                                <asp:BoundField DataField="AccountDescrp" HeaderText="Account Description" SortExpression="AccountDescrp" />
                                <asp:BoundField HeaderText="Date" />
                            </Columns>
                            <EditRowStyle BackColor="#999999" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:KWIConnectionString %>" SelectCommand="SELECT Company, AccountNumber, SubAccount, Referance, SUM(Debit) AS Debit, SUM(Credit) AS Credit, Descrp, AccountDescrp, StoreID FROM View_GLIntMasterSum WHERE (TrxDate BETWEEN @from AND @to ) GROUP BY Company, AccountNumber, SubAccount, Referance, Descrp, AccountDescrp, StoreID ORDER BY StoreID" DeleteCommand="Truncate Table WebGLIntReportLog" InsertCommand="INSERT INTO WebGLIntReportLog(SessionID, Userid, Company, AccountNumber, Subaccount, RefNO, VDate, Debit, Credit, Description, ACcountDescription, BatchNumber, Month, Period, PrepBy) VALUES (@SessionID, @Userid, @Company, @AccountNumber, @Subaccount, @RefNO, @VDate, @Debit, @Credit, @Description, @ACcountDescription, @BatchNumber, @Month, @Period, @PrepBy)">
                            <InsertParameters>
                                <asp:Parameter Name="SessionID" />
                                <asp:Parameter Name="Userid" />
                                <asp:Parameter Name="Company" />
                                <asp:Parameter Name="AccountNumber" />
                                <asp:Parameter Name="Subaccount" />
                                <asp:Parameter Name="RefNO" />
                                <asp:Parameter Name="VDate" />
                                <asp:Parameter Name="Debit" />
                                <asp:Parameter Name="Credit" />
                                <asp:Parameter Name="Description" />
                                <asp:Parameter Name="ACcountDescription" />
                                <asp:Parameter Name="BatchNumber" />
                                <asp:Parameter Name="Month" />
                                <asp:Parameter Name="Period" />
                                <asp:Parameter Name="PrepBy" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:ControlParameter ControlID="TXTDateFrom" Name="from" PropertyName="Text" />
                                <asp:ControlParameter ControlID="TXTDateTo" Name="to" PropertyName="Text" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="COGSDS" runat="server" ConnectionString="<%$ ConnectionStrings:KWIConnectionString %>" DeleteCommand="Truncate Table WebGLIntReportLog" InsertCommand="INSERT INTO WebGLIntReportLog(SessionID, Userid, Company, AccountNumber, Subaccount, RefNO, VDate, Debit, Credit, Description, ACcountDescription, BatchNumber, Month, Period, PrepBy) VALUES (@SessionID, @Userid, @Company, @AccountNumber, @Subaccount, @RefNO, @VDate, @Debit, @Credit, @Description, @ACcountDescription, @BatchNumber, @Month, @Period, @PrepBy)" SelectCommand="SELECT AccountNumber, Company, Descrp, AccountDescrp, SubAccount, StoreID, Referance, SUM([Transaction Price]) AS [Transaction Price], SUM(Cost) AS Cost, SUM(MSRP) AS MSRP, SUM(Markdown) AS Markdown, (CASE WHEN ([AccountNumber]) = 50100 AND SUM(MSRP) &lt;&gt; 0 THEN ((SUM([Cost]) / SUM([MSRP])) * SUM([Transaction Price])) ELSE CASE WHEN AccountNumber = 50120 AND SUM(MSRP) &lt;&gt; 0 THEN ((SUM(MSRP) - SUM([Transaction Price])) * (SUM(Cost) / SUM(MSRP))) ELSE CASE WHEN accountnumber = 52998 THEN (SUM(MSRP) - SUM([Transaction Price])) ELSE 0 END END END) AS Debit, (CASE WHEN ([AccountNumber]) = 16100 THEN (SUM([Cost])) ELSE CASE WHEN accountnumber = 52999 THEN (SUM(MSRP) - SUM([Transaction Price])) ELSE 0 END END) AS Credit, Collect FROM View_CogsGL_Master WHERE (TrxDate BETWEEN @from AND @to ) GROUP BY AccountNumber, Company, Descrp, AccountDescrp, SubAccount, StoreID, Referance, Collect ORDER BY SubAccount">
                            <InsertParameters>
                                <asp:Parameter Name="SessionID" />
                                <asp:Parameter Name="Userid" />
                                <asp:Parameter Name="Company" />
                                <asp:Parameter Name="AccountNumber" />
                                <asp:Parameter Name="Subaccount" />
                                <asp:Parameter Name="RefNO" />
                                <asp:Parameter Name="VDate" />
                                <asp:Parameter Name="Debit" />
                                <asp:Parameter Name="Credit" />
                                <asp:Parameter Name="Description" />
                                <asp:Parameter Name="ACcountDescription" />
                                <asp:Parameter Name="BatchNumber" />
                                <asp:Parameter Name="Month" />
                                <asp:Parameter Name="Period" />
                                <asp:Parameter Name="PrepBy" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:ControlParameter ControlID="TXTDateFrom" Name="from" PropertyName="Text" />
                                <asp:ControlParameter ControlID="TXTDateTo" Name="to" PropertyName="Text" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

    <table class="w-100" __designer:mapid="11e">
        <tr __designer:mapid="11f">
            <td style="width: 130px" __designer:mapid="120">&nbsp;</td>
            <td __designer:mapid="121">&nbsp;</td>
        </tr>
        <tr __designer:mapid="122">
            <td style="width: 130px" __designer:mapid="123">

    <asp:ImageButton ID="ImageButton1" runat="server" Height="48px" ImageUrl="~/Image/Untitled-5-512.png" Width="64px" />
            </td>
            <td __designer:mapid="125">

    <asp:ImageButton ID="ImageButton2" runat="server" Height="48px" ImageUrl="~/Image/pdf-icon.png" Width="51px" Visible="False" />
            </td>
        </tr>
        <tr __designer:mapid="127">
            <td style="width: 130px" __designer:mapid="128">Download CSV</td>
            <td __designer:mapid="129">&nbsp;</td>
        </tr>
    </table>
    <br />

</asp:Content>
