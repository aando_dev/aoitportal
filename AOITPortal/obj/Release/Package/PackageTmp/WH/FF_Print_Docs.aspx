﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="FF_Print_Docs.aspx.vb" Inherits="AOITPortal.FF_Print_Docs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <table class="nav-justified">
            <tr>
                <td colspan="2" style="height: 20px"></td>
            </tr>
            <tr>
                <td colspan="2" style="font-size: xx-large">
                    <span lang="PT"><strong>Farfetch</strong></span><span><strong> Print Documents </strong></span>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="TXT_PickTicket" runat="server" Height="32px" Width="212px"></asp:TextBox>
                    <ajaxToolkit:TextBoxWatermarkExtender ID="TXT_PickTicket_TextBoxWatermarkExtender" runat="server" BehaviorID="TXT_OrderID_TextBoxWatermarkExtender" TargetControlID="TXT_PickTicket" WatermarkText="Enter Pick Ticket No." />
                    <asp:Button ID="BTN_Search" runat="server" BackColor="#4D4F6F" BorderStyle="Solid" Font-Size="Small" ForeColor="White" Height="40px" style="text-align:center" Text="SEARCH" Width="140px" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="text-right">
                    <asp:Button ID="BTN_Search1" runat="server" BackColor="#4D4F6F" BorderStyle="Solid" Font-Size="Small" ForeColor="White" Height="40px" Text="CLEAR FILTER" Width="140px" style="text-align:center"  />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="TXT_OrderID" runat="server" Height="32px" Width="212px"></asp:TextBox>
                    <ajaxToolkit:TextBoxWatermarkExtender ID="TXT_OrderID_TextBoxWatermarkExtender" runat="server" BehaviorID="TXT_OrderID_TextBoxWatermarkExtender" TargetControlID="TXT_OrderID" WatermarkText="Enter PO No." />
                    <asp:Button ID="BTN_Search0" runat="server" BackColor="#4D4F6F" BorderStyle="Solid" Font-Size="Small" ForeColor="White" Height="40px" Text="SEARCH" Width="140px" style="text-align:center"  />
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource_FF_Pick_Doc" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <Columns>
                            <asp:BoundField DataField="PO_NUM" HeaderText="PO No." SortExpression="PO_NUM" />
                            <asp:BoundField DataField="Pick_num" HeaderText="Pick Tick No." SortExpression="Pick_num" />
                            <asp:HyperLinkField DataNavigateUrlFields="Link" DataTextField="hltext" HeaderText="Documents" target="_blank"  />
                        </Columns>
                        <EditRowStyle BackColor="#999999" />
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#4D4F6F" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource_FF_Pick_Doc" runat="server" ConnectionString="<%$ ConnectionStrings:AOViewsConnectionString %>" SelectCommand="select PO_NUM, Pick_num, Replace((ISNULL(finallink, '')),'\\aando.local\public\IT Public\FarFetch\Docs\','~\FF_Docs\') as Link,(case when finallink =null then 'N/A' when finallink ='' then 'N/A' else convert(varchar,Pick_num) End) as hltext  from AO_FF_PICK_DOCS where Shipped is null"></asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
