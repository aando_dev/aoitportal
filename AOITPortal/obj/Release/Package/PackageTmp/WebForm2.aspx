﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WebForm2.aspx.vb" Inherits="AOITPortal.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <script type="text/javascript">

         function incNumber() {
             var c = parseInt(document.getElementById("Text1").value);
             c++;
             document.getElementById("Text1").value = c;
            
         }

         function decNumber() {
             var c = parseInt(document.getElementById("Text1").value);
             c--;
             document.getElementById("Text1").value = c;
            
         }
     </script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
      <input id="Text1" type="text" value="0"/>
    <input type="button" value="Increase" id="inc" onclick="incNumber()" />
    <input type="button" value="Decrease" id="dec" onclick="decNumber()" />

    
    </form>
</body>
</html>
