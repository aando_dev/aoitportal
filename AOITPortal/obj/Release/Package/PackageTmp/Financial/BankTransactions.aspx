﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="BankTransactions.aspx.vb" Inherits="AOITPortal.BankTransactions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="nav-justified">
                <tr>
                    <td style="height: 61px"></td>
                    <td colspan="3" style="height: 61px"></td>
                </tr>
                <tr>
                    <td>From Date </td>
                    <td>
                        <asp:TextBox ID="TXTDateFrom" runat="server" Width="200px"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="TXTDateFrom_CalendarExtender" runat="server" BehaviorID="TXTDateFrom_CalendarExtender" TargetControlID="TXTDateFrom" />
                    </td>
                    <td>To Date</td>
                    <td>
                        <asp:TextBox ID="TXTDateFrom0" runat="server" Width="200px"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="TXTDateFrom0_CalendarExtender" runat="server" BehaviorID="TXTDateFrom_CalendarExtender" TargetControlID="TXTDateFrom0" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 20px">Company</td>
                    <td colspan="3" style="height: 20px">
                        <asp:DropDownList ID="DRPCompany" runat="server" DataSourceID="Comp" DataTextField="Company_ID" DataValueField="StoreID" Width="300px">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="Comp" runat="server" ConnectionString="<%$ ConnectionStrings:KWIConnectionString %>" SelectCommand="SELECT * FROM [Companyids]"></asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px">&nbsp;</td>
                    <td colspan="3" style="height: 20px">&nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 20px">Date</td>
                    <td colspan="3" style="height: 20px">
                        <asp:TextBox ID="TXTTrxDate" runat="server" TabIndex="1" Width="200px"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="TXTTrxDate_CalendarExtender" runat="server" BehaviorID="TXTDateFrom_CalendarExtender" TargetControlID="TXTTrxDate" />
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px">&nbsp;</td>
                    <td colspan="3" style="height: 20px">&nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 20px">Amount</td>
                    <td colspan="3" style="height: 20px">
                        <asp:TextBox ID="TXTAmount" runat="server" TabIndex="2" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="height: 45px">Tender Type</td>
                    <td colspan="3" style="height: 45px">
                        <asp:DropDownList ID="DRPTender" runat="server" DataSourceID="Tender" DataTextField="desc_cd" DataValueField="code_cd" TabIndex="3" Width="300px">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="Tender" runat="server" ConnectionString="<%$ ConnectionStrings:KWIConnectionString %>" SelectCommand="SELECT * FROM [codedesc] WHERE ([type_cd] = @type_cd)">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="FOP" Name="type_cd" Type="String" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td style="height: 10px"></td>
                    <td colspan="3" style="height: 10px"></td>
                </tr>
                <tr>
                    <td>Details</td>
                    <td colspan="3">
                        <asp:TextBox ID="TXTDetails" runat="server" TabIndex="4" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px"></td>
                    <td colspan="3" style="height: 20px">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="3">
                        <asp:Button ID="Button1" runat="server" TabIndex="5" Text="Save" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="BTrxid" DataSourceID="BankTrans" ForeColor="#333333" GridLines="None" Width="100%">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            <Columns>
                                <asp:BoundField DataField="BTrxid" HeaderText="TrxID" InsertVisible="False" ReadOnly="True" SortExpression="BTrxid" />
                                <asp:BoundField DataField="TrxDate" DataFormatString="{0:d}" HeaderText="Date" SortExpression="TrxDate" />
                                <asp:BoundField DataField="TrxAmount" HeaderText="Amount" SortExpression="TrxAmount" />
                                <asp:BoundField DataField="TrxDetails" HeaderText="Details" SortExpression="TrxDetails" />
                                <asp:CommandField ShowDeleteButton="True" />
                            </Columns>
                            <EditRowStyle BackColor="#999999" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="BankTrans" runat="server" ConnectionString="<%$ ConnectionStrings:KWIConnectionString %>" DeleteCommand="DELETE FROM Fin_BankTransactios WHERE (BTrxid = @BtrxID)" InsertCommand="INSERT INTO Fin_BankTransactios(EntryDate, UserID, WebSession, TrxDate, TrxAmount, TrxDetails, TenderType, CompID) VALUES (GETDATE(), 1, 'T', @TrxDate, @TrxAmount, @TrxDetails, @TenderType, @CompID)" SelectCommand="SELECT * FROM [Fin_BankTransactios]">
                            <DeleteParameters>
                                <asp:ControlParameter ControlID="GridView1" Name="BtrxID" PropertyName="SelectedValue" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:ControlParameter ControlID="TXTTrxDate" Name="TrxDate" PropertyName="Text" />
                                <asp:ControlParameter ControlID="TXTAmount" Name="TrxAmount" PropertyName="Text" />
                                <asp:ControlParameter ControlID="TXTDetails" Name="TrxDetails" PropertyName="Text" />
                                <asp:ControlParameter ControlID="DRPTender" Name="TenderType" PropertyName="SelectedValue" />
                                <asp:ControlParameter ControlID="DRPCompany" Name="CompID" PropertyName="SelectedValue" />
                            </InsertParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="3">&nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
