﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="OrderDetails.aspx.vb" Inherits="AOITPortal.OrderDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="nav-justified">
                <tr>
                    <td class="text-center" colspan="2" style="color: #003399">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center" colspan="2" style="color: #003399">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center" colspan="2" style="color: #003399"><strong>ACDC Order Details </strong></td>
                </tr>
                <tr>
                    <td class="text-center" colspan="2" style="color: #003399">&nbsp;</td>
                </tr>
                <tr>
                    <td>Order Number</td>
                    <td>
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                        <asp:Button ID="Button1" runat="server" Text="Find Order" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <asp:Image ID="Image1" runat="server" Height="52px" ImageUrl="~/Image/SkinnySeveralAsianlion.gif" Width="56px" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" CellPadding="4" DataSourceID="ACDCOrderDetails" ForeColor="#333333" GridLines="None" Height="50px" Width="100%">
                            <AlternatingRowStyle BackColor="White" />
                            <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
                            <EditRowStyle BackColor="#2461BF" />
                            <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
                            <Fields>
                                <asp:BoundField DataField="OrderNumber" HeaderText="Order Number" SortExpression="OrderNumber" />
                                <asp:BoundField DataField="OrderSource" HeaderText="Order Source" SortExpression="OrderSource" />
                                <asp:BoundField DataField="OrderDate" HeaderText="Order Date" SortExpression="OrderDate" />
                                <asp:BoundField DataField="OrderSalesTax" HeaderText="Order Sales Tax" SortExpression="OrderSalesTax" />
                                <asp:BoundField DataField="OrderStore" HeaderText="Order Store" ReadOnly="True" SortExpression="OrderStore" />
                                <asp:BoundField DataField="OrderShipCharge" HeaderText="Order Ship Charge" SortExpression="OrderShipCharge" />
                                <asp:BoundField DataField="MerchandiseOrderTotal" HeaderText="Order Total" SortExpression="MerchandiseOrderTotal" />
                                <asp:BoundField DataField="Bill_To_FN" HeaderText="Bill_To_FN" SortExpression="Bill_To_FN" />
                                <asp:BoundField DataField="Bill_To_MN" HeaderText="Bill_To_MN" SortExpression="Bill_To_MN" />
                                <asp:BoundField DataField="Bill_To_LN" HeaderText="Bill_To_LN" SortExpression="Bill_To_LN" />
                                <asp:BoundField DataField="Bill_To_Address1" HeaderText="Bill_To_Address1" SortExpression="Bill_To_Address1" />
                                <asp:BoundField DataField="Bill_To_Address2" HeaderText="Bill_To_Address2" SortExpression="Bill_To_Address2" />
                                <asp:BoundField DataField="Bill_To_Address3" HeaderText="Bill_To_Address3" SortExpression="Bill_To_Address3" />
                                <asp:BoundField DataField="Bill_To_City" HeaderText="Bill_To_City" SortExpression="Bill_To_City" />
                                <asp:BoundField DataField="Bill_To_State" HeaderText="Bill_To_State" SortExpression="Bill_To_State" />
                                <asp:BoundField DataField="Bill_To_Zipcode" HeaderText="Bill_To_Zipcode" SortExpression="Bill_To_Zipcode" />
                                <asp:BoundField DataField="Bill_To_Country" HeaderText="Bill_To_Country" SortExpression="Bill_To_Country" />
                                <asp:BoundField DataField="Bill_To_Tel" HeaderText="Bill_To_Tel" SortExpression="Bill_To_Tel" />
                                <asp:BoundField DataField="Bill_To_Email" HeaderText="Bill_To_Email" SortExpression="Bill_To_Email" />
                                <asp:BoundField DataField="PaymentType1" HeaderText="Payment Type1" SortExpression="PaymentType1" />
                                <asp:BoundField DataField="PaymentType1Number" HeaderText="Payment Type1 Number" SortExpression="PaymentType1Number" />
                                <asp:BoundField DataField="PaymentType1Expires" HeaderText="Payment Type1 Expires" SortExpression="PaymentType1Expires" />
                                <asp:BoundField DataField="PaymentMethod" HeaderText="Payment Method" SortExpression="PaymentMethod" />
                            </Fields>
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#EFF3FB" />
                        </asp:DetailsView>
                        <asp:SqlDataSource ID="ACDCOrderDetails" runat="server" ConnectionString="<%$ ConnectionStrings:ACDCConnectionString %>" SelectCommand="SELECT * FROM [View_ACD_to_KWI_Order] WHERE ([OrderNumber] = @OrderNumber)">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="TextBox1" Name="OrderNumber" PropertyName="Text" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
