﻿<%@ Page Title="" Language="vb" AutoEventWireup="false"   MasterPageFile="~/Site.Master" CodeBehind="SampleSales.aspx.vb" Inherits="AOITPortal.SampleSales" %>
<%@ Register assembly="FastReport.Web, Version=2018.3.31.0, Culture=neutral, PublicKeyToken=db7e5ce63278458c" namespace="FastReport.Web" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" DefaultButton="BtnScan" runat="server">
                <table class="nav-justified">
                    <tr>
                        <td class="text-center" colspan="7" style="color: #000099; font-size: large; height: 79px"></td>
                    </tr>
                    <tr>
                        <td class="text-center" colspan="7" style="color: #000099; font-size: large; height: 25px"><strong>Sample Sale Transactions</strong></td>
                    </tr>
                    <tr>
                        <td class="text-center" colspan="7" style="color: #000099; font-size: large; height: 25px"></td>
                    </tr>
                    <tr>
                        <td style="height: 22px">Transaction#</td>
                        <td style="height: 22px" colspan="2">
                            <asp:TextBox ID="TXTTransID" runat="server" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td class="text-left" colspan="4" style="height: 22px">
                            <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
                            <br />
                            <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 22px">&nbsp;</td>
                        <td style="height: 22px" colspan="2">&nbsp;</td>
                        <td class="text-left" colspan="4" style="height: 22px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="height: 26px">First Name</td>
                        <td style="height: 26px" colspan="2">
                            <asp:TextBox ID="TXTFN" runat="server" Width="300px"></asp:TextBox>
                        </td>
                        <td class="text-left" style="height: 26px" colspan="2">Last Name</td>
                        <td style="height: 26px">
                            <asp:TextBox ID="TXTLN" runat="server" Width="300px"></asp:TextBox>
                        </td>
                        <td style="height: 26px">
                            <asp:Button ID="BtnNew" runat="server" Text="New Sale" />
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 20px"></td>
                        <td style="height: 20px" colspan="2"><strong>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TXTFN" ErrorMessage="Please Enter First Name" style="color: #FF0000"></asp:RequiredFieldValidator>
                            </strong></td>
                        <td class="text-left" style="height: 20px" colspan="2"></td>
                        <td style="height: 20px">&nbsp;</td>
                        <td style="height: 20px"></td>
                    </tr>
                    <tr>
                        <td style="height: 26px">Sacn UPC</td>
                        <td colspan="2" style="height: 26px">
                            <asp:TextBox ID="TXTUPC" runat="server" Width="300px"></asp:TextBox>
                        </td>
                        <td class="text-left" colspan="2" style="height: 26px">
                            Price
                            <asp:TextBox ID="TXTPrice" runat="server" Width="40px">35.00</asp:TextBox>
                        </td>
                        <td style="height: 26px">
                            <asp:Button ID="BtnScan" runat="server" Text="Sacn" />
                        </td>
                        <td style="height: 26px">
                            <asp:Button ID="BtnNew0" runat="server" Text="End Sale" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="2">&nbsp;</td>
                        <td class="text-left" colspan="2">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Load Transaction: </td>
                        <td colspan="2">
                            <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource2" DataTextField="TransID" DataValueField="TransID" Height="22px" Width="100px">
                            </asp:DropDownList>
                        </td>
                        <td class="text-left" colspan="2">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Button ID="Button1" runat="server" Text="Load Transaction" />
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 20px" colspan="7">
                            </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 20px">
                            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/SampleSale/SampleSalesReport.aspx" Target="_blank">Sample Sale Transaction Report</asp:HyperLink>
                        </td>
                        <td colspan="2" style="height: 20px">
                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:AOWebPortalConnectionString %>" SelectCommand="SELECT * FROM [SampleSales_TransMaster]"></asp:SqlDataSource>
                        </td>
                        <td colspan="3" style="height: 20px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="7" style="height: 20px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="7" style="height: 20px">
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/SampleSale/PrintSampleSaleRecipt.aspx" Target="_blank" Visible="False">View Report</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="TransID" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" Width="100%">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                <Columns>
                                    <asp:BoundField DataField="TrnsDetID" HeaderText="ID" SortExpression="TrnsDetID" />
                                    <asp:BoundField DataField="UPC" HeaderText="UPC" SortExpression="UPC" />
                                    <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" />
                                    <asp:BoundField DataField="division" HeaderText="Division" SortExpression="division" />
                                    <asp:BoundField DataField="style_name" HeaderText="Style" SortExpression="style_name" />
                                    <asp:BoundField DataField="color_name" HeaderText="Color" SortExpression="color_name" />
                                    <asp:BoundField DataField="Size" HeaderText="Size" SortExpression="Size" />
                                    <asp:TemplateField HeaderText="Image">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Eval("Image") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Image ID="Image1" Width="100px" Height= "100px" runat="server" ImageUrl='<%# Eval("Image") %>' />
                                        </ItemTemplate>
                                      
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:Button ID="DeleteCart" runat="server" Width="25px" style="background-image:url(../Image/Delete.png);background-repeat:no-repeat; background-position:center; background-size:20px;" CommandArgument="<%# CType(Container, GridViewRow).RowIndex %>" CommandName="DeleteCart" Text="" />
                                </ItemTemplate>
                            </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#999999" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:AOWebPortalConnectionString %>" SelectCommand="SELECT TransID, TransDate, EmpFN, EmpLN, email, phone, TrnsDetID, UPC, Price, CreatedDT, CreatedUser, CreatedPC, DeletedDT, DeletedUser, DeletedPC, IsDeleted, division, STYLE, style_name, COLOR_CODE, Size, color_name, Label, Wholesale_Price, STD_COST, Retail_Price, Image FROM View_SampleSale_TransactionsDetails WHERE (TransID = @TransID) AND (IsDeleted = @IsDeleted) ORDER BY TrnsDetID DESC">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="TXTTransID" Name="TransID" PropertyName="Text" Type="Decimal" />
                                    <asp:Parameter DefaultValue="0" Name="IsDeleted" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="6">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="6">&nbsp;</td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>

    </asp:UpdatePanel>
        
</asp:Content>
