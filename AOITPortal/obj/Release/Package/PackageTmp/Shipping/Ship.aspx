﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"  CodeBehind="Ship.aspx.vb" Inherits="AOITPortal.Ship" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1"  DefaultButton="Button1" runat="server">
                <table class="nav-justified">
                    <tr>
                        <td class="text-center" colspan="2" style="height: 20px">&nbsp;</td>
                        <td class="text-center" style="height: 20px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="text-center" colspan="2" style="height: 20px; font-size: x-large"><strong>Create Shipment</strong></td>
                        <td class="text-center" style="height: 20px"></td>
                    </tr>
                    <tr>
                        <td class="text-center" colspan="2" style="height: 20px"></td>
                        <td class="text-center" style="height: 20px"></td>
                    </tr>
                    <tr>
                        <td class="text-center" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="height: 20px; width: 349px;">Scan Carton Number</td>
                        <td style="height: 20px; width: 135px;">
                            <asp:TextBox ID="TXTCartonNum" runat="server" Width="479px"></asp:TextBox>
                        </td>
                        <td class="text-left" style="height: 20px">
                            <asp:Button ID="Button1" runat="server" Text="Sacn" />
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 20px; width: 349px;">&nbsp;</td>
                        <td style="height: 20px; width: 135px;">
                            <asp:Label ID="Label1" runat="server" Font-Bold="True"></asp:Label>
                        </td>
                        <td style="height: 20px">
                            <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank">Print Label</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="height: 20px">
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                <ProgressTemplate>
                                    <asp:Image ID="Image1" runat="server" Height="52px" ImageUrl="~/Image/SkinnySeveralAsianlion.gif" Width="56px" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 20px; width: 349px;"></td>
                        <td style="height: 20px; width: 135px;"></td>
                        <td style="height: 20px"></td>
                    </tr>
                    <tr>
                        <td style="height: 14px; width: 349px;"></td>
                        <td style="height: 14px; width: 135px;">&nbsp;</td>
                        <td style="height: 14px"></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="height: 20px">
                            <asp:DetailsView ID="DetailsView2" runat="server" AutoGenerateRows="False" CellPadding="4" DataSourceID="Shipped" ForeColor="#333333" GridLines="None" Height="50px" Width="100%">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                <CommandRowStyle BackColor="#E2DED6" Font-Bold="True" />
                                <EditRowStyle BackColor="#999999" />
                                <FieldHeaderStyle BackColor="#E9ECF1" Font-Bold="True" />
                                <Fields>
                                    <asp:BoundField DataField="carton_num" HeaderText="Carton Number" SortExpression="carton_num" />
                                    <asp:BoundField DataField="pick_num" HeaderText="Pick Number" SortExpression="pick_num" />
                                    <asp:BoundField DataField="po_num" HeaderText="P.O Number" SortExpression="po_num" />
                                    <asp:BoundField DataField="ShipmentID" HeaderText="Shipment ID" SortExpression="ShipmentID" />
                                    <asp:BoundField DataField="ShippedDateandTime" HeaderText="Shipped Date and Time" SortExpression="ShippedDateandTime" />
                                    <asp:BoundField DataField="ShippedUser" HeaderText="User" SortExpression="ShippedUser" />
                                    <asp:BoundField DataField="ShippedPC" HeaderText="Device" SortExpression="ShippedPC" />
                                    <asp:BoundField DataField="Customer" HeaderText="Customer ID" SortExpression="Customer" />
                                    <asp:BoundField DataField="Cust_Name" HeaderText="Customer Name" SortExpression="Cust_Name" />
                                    <asp:BoundField DataField="sh_Addr2" HeaderText="Address 1" SortExpression="sh_Addr2" />
                                    <asp:BoundField DataField="sh_city" HeaderText="Address 2" SortExpression="sh_city" />
                                    <asp:BoundField DataField="sh_state" HeaderText="State" SortExpression="sh_state" />
                                    <asp:BoundField DataField="sh_zip" HeaderText="Zip Code" SortExpression="sh_zip" />
                                    <asp:BoundField DataField="sh_country" HeaderText="Country" SortExpression="sh_country" />
                                    <asp:BoundField DataField="sh_contact" HeaderText="Contact" SortExpression="sh_contact" />
                                    <asp:BoundField DataField="sh_phone" HeaderText="Phone" SortExpression="sh_phone" />
                                    <asp:BoundField DataField="sh_email" HeaderText="Email" SortExpression="sh_email" />
                                    <asp:BoundField DataField="ship_from_company" HeaderText="Shipped From Company" SortExpression="ship_from_company" />
                                    <asp:BoundField DataField="ship_from_contact_name" HeaderText="Shipped From Name" SortExpression="ship_from_contact_name" />
                                    <asp:BoundField DataField="ship_from_add1" HeaderText="Shipped From Address 1" SortExpression="ship_from_add1" />
                                    <asp:BoundField DataField="ship_from_add2" HeaderText="Shipped From Address 2" SortExpression="ship_from_add2" />
                                    <asp:BoundField DataField="ship_from_city" HeaderText="Shipped From City" SortExpression="ship_from_city" />
                                    <asp:BoundField DataField="ship_from_state" HeaderText="Shipped From State" SortExpression="ship_from_state" />
                                    <asp:BoundField DataField="ship_from_country" HeaderText="Shipped From Country" SortExpression="ship_from_country" />
                                    <asp:BoundField DataField="ship_from_zip" HeaderText="Shipped From Zip Code" SortExpression="ship_from_zip" />
                                    <asp:BoundField DataField="ship_from_phone" HeaderText="Shipeed From Phone" SortExpression="ship_from_phone" />
                                    <asp:BoundField DataField="ship_from_email" HeaderText="Shipped From Email" SortExpression="ship_from_email" />
                                </Fields>
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            </asp:DetailsView>
                            <asp:SqlDataSource ID="Shipped" runat="server" ConnectionString="<%$ ConnectionStrings:freightpopConnectionString %>" SelectCommand="SELECT * FROM [ShippedCartons] WHERE ([carton_num] = @carton_num)">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="TXTCartonNum" Name="carton_num" PropertyName="Text" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 20px; width: 349px;"></td>
                        <td style="height: 20px; width: 135px;"></td>
                        <td style="height: 20px"></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 20px">&nbsp;</td>
                        <td style="height: 20px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 349px">&nbsp;</td>
                        <td style="width: 135px">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 349px">&nbsp;</td>
                        <td style="width: 135px">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 349px">&nbsp;</td>
                        <td style="width: 135px">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
