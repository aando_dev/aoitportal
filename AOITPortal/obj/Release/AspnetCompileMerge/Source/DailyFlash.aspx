﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="DailyFlash.aspx.vb" Inherits="AOITPortal.DailyFlash" %>
<%@ Register assembly="FastReport.Web, Version=2018.3.31.0, Culture=neutral, PublicKeyToken=db7e5ce63278458c" namespace="FastReport.Web" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="w-100" style="margin-bottom: 0px">
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:KWIConnectionString %>" SelectCommand="SELECT Date, StoreCode, GrossSales, TransactionalReturns, MerchandiseReturns, EOH, NetTransSales, NetMerchSales, StoreType, StoreStatus, Month, Week, Season, EventDay, EventWeek, BestDayStore, DaySubtotal, BestDaySubTotal, TotalWeek, BestweekStore, WeekSubtotal, BestWeekSubtotal, TotalMonthStore, BsetMonthStore, MonthSubtotal, BestMonthSubtotal FROM View_BloomDailyFlashSum3 WHERE (Date BETWEEN @Date AND @Date2)">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="TXTDateFrom0" Name="Date" PropertyName="Text" />
                                <asp:ControlParameter ControlID="TXTDateTo0" Name="Date2" PropertyName="Text" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td style="height: 26px">&nbsp;</td>
                    <td style="height: 26px">&nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 26px">Date From</td>
                    <td style="height: 26px">
                        <asp:TextBox ID="TXTDateFrom0" runat="server" Width="200px"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="TXTDateFrom0_CalendarExtender" runat="server" BehaviorID="TXTDateFrom_CalendarExtender" TargetControlID="TXTDateFrom0" />
                    </td>
                </tr>
                <tr>
                    <td style="height: 26px">&nbsp;</td>
                    <td style="height: 26px">&nbsp;</td>
                </tr>
                <tr>
                    <td>Date To</td>
                    <td>
                        <asp:TextBox ID="TXTDateTo0" runat="server" Width="200px"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="TXTDateTo0_CalendarExtender" runat="server" BehaviorID="TXTDateTo_CalendarExtender" TargetControlID="TXTDateTo0" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 36px"></td>
                    <td style="height: 36px">
                        <asp:ImageButton ID="ImageButton1" runat="server" Height="34px" ImageUrl="~/Image/submit_button.png" Width="106px" />
                    </td>
                </tr>
                <tr>
                    <td style="height: 36px">
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <asp:Image ID="Image1" runat="server" Height="52px" ImageUrl="~/Image/SkinnySeveralAsianlion.gif" Width="60px" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                    <td style="height: 36px">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 36px">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            <Columns>
                                <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" />
                                <asp:BoundField DataField="StoreCode" HeaderText="StoreCode" SortExpression="StoreCode" />
                                <asp:BoundField DataField="GrossSales" HeaderText="GrossSales" SortExpression="GrossSales" />
                                <asp:BoundField DataField="TransactionalReturns" HeaderText="TransactionalReturns" SortExpression="TransactionalReturns" />
                                <asp:BoundField DataField="MerchandiseReturns" HeaderText="MerchandiseReturns" SortExpression="MerchandiseReturns" />
                                <asp:BoundField DataField="EOH" HeaderText="EOH" SortExpression="EOH" />
                                <asp:BoundField DataField="NetTransSales" HeaderText="NetTransSales" SortExpression="NetTransSales" />
                                <asp:BoundField DataField="NetMerchSales" HeaderText="NetMerchSales" ReadOnly="True" SortExpression="NetMerchSales" />
                                <asp:BoundField DataField="StoreType" HeaderText="StoreType" SortExpression="StoreType" />
                                <asp:BoundField DataField="StoreStatus" HeaderText="StoreStatus" SortExpression="StoreStatus" />
                                <asp:BoundField DataField="Month" HeaderText="Month" ReadOnly="True" SortExpression="Month" />
                                <asp:BoundField DataField="Week" HeaderText="Week" ReadOnly="True" SortExpression="Week" />
                                <asp:BoundField DataField="Season" HeaderText="Season" ReadOnly="True" SortExpression="Season" />
                                <asp:BoundField DataField="EventDay" HeaderText="EventDay" ReadOnly="True" SortExpression="EventDay" />
                                <asp:BoundField DataField="EventWeek" HeaderText="EventWeek" ReadOnly="True" SortExpression="EventWeek" />
                                <asp:BoundField DataField="BestDayStore" HeaderText="BestDayStore" ReadOnly="True" SortExpression="BestDayStore" />
                                <asp:BoundField DataField="DaySubtotal" HeaderText="DaySubtotal" ReadOnly="True" SortExpression="DaySubtotal" />
                                <asp:BoundField DataField="BestDaySubTotal" HeaderText="BestDaySubTotal" ReadOnly="True" SortExpression="BestDaySubTotal" />
                                <asp:BoundField DataField="TotalWeek" HeaderText="TotalWeek" ReadOnly="True" SortExpression="TotalWeek" />
                                <asp:BoundField DataField="BestweekStore" HeaderText="BestweekStore" ReadOnly="True" SortExpression="BestweekStore" />
                                <asp:BoundField DataField="WeekSubtotal" HeaderText="WeekSubtotal" ReadOnly="True" SortExpression="WeekSubtotal" />
                                <asp:BoundField DataField="BestWeekSubtotal" HeaderText="BestWeekSubtotal" ReadOnly="True" SortExpression="BestWeekSubtotal" />
                                <asp:BoundField DataField="TotalMonthStore" HeaderText="TotalMonthStore" ReadOnly="True" SortExpression="TotalMonthStore" />
                                <asp:BoundField DataField="BsetMonthStore" HeaderText="BsetMonthStore" ReadOnly="True" SortExpression="BsetMonthStore" />
                                <asp:BoundField DataField="MonthSubtotal" HeaderText="MonthSubtotal" ReadOnly="True" SortExpression="MonthSubtotal" />
                                <asp:BoundField DataField="BestMonthSubtotal" HeaderText="BestMonthSubtotal" ReadOnly="True" SortExpression="BestMonthSubtotal" />
                            </Columns>
                            <EditRowStyle BackColor="#999999" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
