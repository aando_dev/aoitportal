﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Counter.ascx.vb" Inherits="AOITPortal.Counter" %>
<style type="text/css">
    .auto-style1 {
        font-weight: bold;
        font-size: x-large;
    }
    .auto-style2 {
        font-size: x-large;
    }
</style>
<table>
                                                <tr>
                                                    <td>
                                                        <strong>
                                                        <asp:Button ID="BTN_Minus" runat="server" BackColor="#4D4F6F" BorderStyle="Solid" ForeColor="White" Height="40px" Text="-" Width="40px" OnClick="Button1_Click" CssClass="auto-style1"/>
                                                        </strong>
                                                    </td>
                                                    <td>
                                                        <strong>
                                                        <asp:TextBox ID="TXT_Number" runat="server" Height="40px" Text="0" Width="40px" style="text-align: center" CssClass="auto-style2" ReadOnly="True"></asp:TextBox>
                                                        </strong>
                                                    </td>
                                                    <td>
                                                        <strong>
                                                        <asp:Button ID="BTN_Plus" runat="server" BackColor="#4D4F6F" BorderStyle="Solid" ForeColor="White" Height="40px" Text="+" Width="40px" CssClass="auto-style1" />
                                                        </strong>
                                                    </td>
                                                </tr>
                                            </table>