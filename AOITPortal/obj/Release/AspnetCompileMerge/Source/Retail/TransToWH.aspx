﻿     <%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="TransToWH.aspx.vb" Inherits="AOITPortal.TransToWH" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server">
                <table class="nav-justified">
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="height: 20px"></td>
                        <td style="height: 20px" colspan="4">
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center" colspan="5" style="font-size: large"><strong>Transfere to Warehouse</strong></td>
                    </tr>
                    <tr>
                        <td class="text-center" colspan="5" style="font-size: large; height: 25px;"></td>
                    </tr>
                    <tr>
                        <td style="height: 22px">Search</td>
                        <td style="height: 22px">
                            <asp:TextBox ID="TXTSearch" runat="server" Width="179px"></asp:TextBox>
                        </td>
                        <td style="height: 22px" class="text-center">
                            By</td>
                        <td style="height: 22px">
                            <asp:DropDownList ID="DropDownList2" runat="server" Width="200">
                                <asp:ListItem Selected="True" Value="0">UPC</asp:ListItem>
                                <asp:ListItem Value="1">Style</asp:ListItem>
                                <asp:ListItem Value="2">Color</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Button ID="Button2" runat="server" Text="Search" />
                        </td>
                        <td style="height: 22px"></td>
                    </tr>
                    <tr>
                        <td style="height: 22px">&nbsp;</td>
                        <td style="height: 22px" colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="height: 22px">To Store</td>
                        <td style="height: 22px" colspan="4">
                            <asp:DropDownList ID="DropDownList1" Width="300px" runat="server" DataSourceID="Stores" DataTextField="st_num" DataValueField="st_num" Height="22px">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="Stores" runat="server" ConnectionString="<%$ ConnectionStrings:beowulfConnectionString %>" SelectCommand="SELECT Rowid, st_num, st_nam, st_ad, st_cy, st_st, st_zp, st_tel, st_poll, st_clnt, st_rep, st_sz, st_odt, st_num2, st_size, st_qty, st_ex, st_email, st_reg, st_odt2, st_num3, st_rep1, st_rep2, st_rep3, st_num4, st_tax, st_mod1, st_mod2, st_mod3, st_cttax, st_sttax, st_thrsh, st_dflt, st_siz, st_bnam, st_bnum, st_nod, st_tm, st_close, st_man, st_vol, st_invd, st_shk, st_ad2, st_cs, st_vol2, st_fax, st_cap, st_stock, st_foot, st_fdescr, st_cou, st_thrsh2, st_tax2, st_sndsal, st_regos, st_csgnum, st_webinv, st_consign, st_mposonly, st_gl1, st_gl2, st_gl3, st_gl4, st_gl5, st_ad3, st_qualid, st_social_media1, st_social_media2, st_social_media3 FROM stores WHERE (st_num IN (231776, 231999))"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 22px">&nbsp;</td>
                        <td colspan="4" style="height: 22px">
                            <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="height: 52px">
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                <ProgressTemplate>
                                    <asp:Image ID="Image1" runat="server" Height="52px" ImageUrl="~/Image/SkinnySeveralAsianlion.gif" Width="56px" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="height: 143px">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="TransToWHDS" ForeColor="#333333" GridLines="None" Width="100%">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                <Columns>
                                    <asp:BoundField DataField="Org_Store" HeaderText="Store" SortExpression="Org_Store" ReadOnly="True" />
                                    <asp:BoundField DataField="UPC" HeaderText="UPC" SortExpression="UPC" ReadOnly="True" />
                                    <asp:BoundField DataField="QOH" HeaderText="On Hand Qnty" SortExpression="QOH" ReadOnly="True" />
                                    <asp:BoundField DataField="Pending_Qnty" HeaderText="Pending Qnty" SortExpression="Pending_Qnty" ReadOnly="True" />
                                    <asp:BoundField DataField="Available_Qnty" HeaderText="Available Qnty" ReadOnly="True" SortExpression="Available_Qnty" />
                                    <asp:BoundField DataField="Style" HeaderText="Style" SortExpression="Style" ReadOnly="True" />
                                    <asp:BoundField DataField="Color_Code" HeaderText="Color Code" SortExpression="Color_Code" ReadOnly="True" />
                                    <asp:BoundField DataField="Color_Name" HeaderText="Color Name" SortExpression="Color_Name" ReadOnly="True" />
                                    <asp:BoundField DataField="Size" HeaderText="Size" SortExpression="Size" ReadOnly="True" />
                                    
                                    <asp:BoundField HeaderText="Qnty" >
                                    
                                    <ControlStyle Width="30px" />
                                    <FooterStyle Width="30px" />
                                    <HeaderStyle Width="30px" />
                                    <ItemStyle Width="30px" />
                                    </asp:BoundField>
                                   <asp:CommandField ShowEditButton="True" UpdateText="Transfere" />
                                    
                                </Columns>
                                <EditRowStyle BackColor="#999999" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="TransToWHDS" runat="server" ConnectionString="<%$ ConnectionStrings:AOWebPortalConnectionString %>" SelectCommand="SELECT Org_Store, UPC, QOH, Pending_Qnty, Style, Color_Code, Color_Name, Size, Available_Qnty FROM View_ForTransToWH WHERE (UPC =@UPC)" ProviderName="<%$ ConnectionStrings:AOWebPortalConnectionString.ProviderName %>" EnableViewState="False" InsertCommand="INSERT INTO TransToWHTransactions(UPC, Quantity, FromLocation, ToLocation, DateTransferred, Time, UserID) VALUES (@UPC, @Quantity, @FromLocation, @ToLocation, GETDATE(), @Time, @UserID)" UpdateCommand="UPDATE LoginLogs SET LoginDT = FetDate() WHERE (LogID = 0)">
                                <InsertParameters>
                                    <asp:Parameter Name="UPC" />
                                    <asp:Parameter Name="Quantity" />
                                    <asp:Parameter Name="FromLocation" />
                                    <asp:ControlParameter ControlID="DropDownList1" Name="ToLocation" PropertyName="SelectedValue" />
                                    <asp:Parameter Name="Time" />
                                    <asp:Parameter Name="UserID" />
                                </InsertParameters>
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="TXTSearch" Name="UPC" PropertyName="Text" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
