﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="EmpCommaspx.aspx.vb" Inherits="AOITPortal.EmpCommaspx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table class="nav-justified" __designer:mapid="23">
        <tr __designer:mapid="24">
            <td __designer:mapid="25" style="height: 62px"></td>
            <td style="width: 674px; height: 62px;" __designer:mapid="26"></td>
        </tr>
        <tr __designer:mapid="24">
            <td __designer:mapid="25">Date From</td>
            <td style="width: 674px" __designer:mapid="26">
                <asp:TextBox ID="TXTDateFrom" runat="server" Width="200px"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="TXTDateFrom_CalendarExtender" runat="server" BehaviorID="TXTDateFrom_CalendarExtender" TargetControlID="TXTDateFrom" />
            </td>
        </tr>
        <tr __designer:mapid="24">
            <td __designer:mapid="25" style="height: 20px"></td>
            <td style="width: 674px; height: 20px;" __designer:mapid="26"></td>
        </tr>
        <tr __designer:mapid="24">
            <td __designer:mapid="25">Date To</td>
            <td style="width: 674px" __designer:mapid="26">
                <asp:TextBox ID="TXTDateTo" runat="server" Width="200px"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="TXTDateTo_CalendarExtender" runat="server" BehaviorID="TXTDateTo_CalendarExtender" TargetControlID="TXTDateTo" />
            </td>
        </tr>
        <tr __designer:mapid="24">
            <td __designer:mapid="25">&nbsp;</td>
            <td style="width: 674px" __designer:mapid="26">&nbsp;</td>
        </tr>
        <tr __designer:mapid="24">
            <td __designer:mapid="25">&nbsp;</td>
            <td style="width: 674px" __designer:mapid="26">
                <asp:DropDownList ID="DropDownList3" runat="server" DataSourceID="SotresDS" DataTextField="st_num" DataValueField="st_num" Width="200px" AutoPostBack="True">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SotresDS" runat="server" ConnectionString="<%$ ConnectionStrings:KWIConnectionString %>" SelectCommand="SELECT * FROM [stores]"></asp:SqlDataSource>
                <br />
                <asp:SqlDataSource ID="SotresDS0" runat="server" ConnectionString="<%$ ConnectionStrings:KWIConnectionString %>" SelectCommand="SELECT st_nam FROM stores WHERE (st_num = @st_num)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownList3" Name="st_num" PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataSourceID="SotresDS0" Height="25px" Width="261px">
                    <Fields>
                        <asp:BoundField DataField="st_nam" ShowHeader="False" />
                    </Fields>
                </asp:DetailsView>
            </td>
        </tr>
        <tr __designer:mapid="27">
            <td style="height: 20px" __designer:mapid="28">Select Store</td>
            <td style="height: 20px; width: 674px;" __designer:mapid="29">&nbsp;</td>
        </tr>
        <tr __designer:mapid="27">
            <td style="height: 20px" __designer:mapid="28">Select Empolyee</td>
            <td style="height: 20px; width: 674px;" __designer:mapid="29">
                <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="EmpDS" DataTextField="nam_em" DataValueField="num_em" Width="200px" AutoPostBack="True">
                </asp:DropDownList>
                <asp:SqlDataSource ID="EmpDS" runat="server" ConnectionString="<%$ ConnectionStrings:KWIConnectionString %>" SelectCommand="SELECT rowid, num_em, nam_em, dpt_em, code_em, hrt_em, prt_em, com1_em, com2_em, stc_em, ed_em, avgs_em, ss_em, add1_em, add2_em, ct_em, st_em, zip_em, ms_em, dep_em, ben_em, hire_em, pos_em, posit_em, hdt_em, pass_em, seclv_em, freq_em, idbar_em, act_em, group_em, lang_em, stg_em, email_em FROM employee WHERE (stc_em = @stc_em)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownList3" Name="stc_em" PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <br />
            </td>
        </tr>
        <tr __designer:mapid="3c">
            <td style="height: 20px" __designer:mapid="3d">&nbsp;</td>
            <td style="height: 20px; width: 674px;" __designer:mapid="3e">&nbsp;</td>
        </tr>
        <tr __designer:mapid="3f">
            <td style="height: 20px" __designer:mapid="40">Commiission</td>
            <td style="height: 20px; width: 674px;" __designer:mapid="41">
                <asp:TextBox ID="TXTCom" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr __designer:mapid="43">
            <td style="height: 20px" __designer:mapid="44">&nbsp;</td>
            <td style="height: 20px; width: 674px;" __designer:mapid="45">&nbsp;</td>
        </tr>
        <tr __designer:mapid="46">
            <td style="height: 20px" __designer:mapid="47">Additional Copensation </td>
            <td style="height: 20px; width: 674px;" __designer:mapid="48">
                <asp:TextBox ID="TXTAddCom" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr __designer:mapid="46">
            <td style="height: 20px" __designer:mapid="47"></td>
            <td style="height: 20px; width: 674px;" __designer:mapid="48"></td>
        </tr>
        <tr __designer:mapid="46">
            <td style="height: 20px" __designer:mapid="47">Increase In regular Rate</td>
            <td style="height: 20px; width: 674px;" __designer:mapid="48">
                <asp:TextBox ID="TXTIncinRegRate" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr __designer:mapid="46">
            <td style="height: 20px" __designer:mapid="47"></td>
            <td style="height: 20px; width: 674px;" __designer:mapid="48"></td>
        </tr>
        <tr __designer:mapid="46">
            <td style="height: 20px" __designer:mapid="47">New Increase OT Rate</td>
            <td style="height: 20px; width: 674px;" __designer:mapid="48">
                <asp:TextBox ID="TXTNewIncOTRate" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr __designer:mapid="46">
            <td style="height: 20px" __designer:mapid="47">&nbsp;</td>
            <td style="height: 20px; width: 674px;" __designer:mapid="48">&nbsp;</td>
        </tr>
        <tr __designer:mapid="46">
            <td style="height: 20px" __designer:mapid="47">Addtional Wages Owed</td>
            <td style="height: 20px; width: 674px;" __designer:mapid="48">
                <asp:TextBox ID="TXTAddWages" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr __designer:mapid="4a">
            <td style="height: 20px" __designer:mapid="4b">Weeks as of Retail<br />
            </td>
            <td style="height: 20px; width: 674px;" __designer:mapid="4c">
                <asp:TextBox ID="TXTWeeks" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr __designer:mapid="4a">
            <td style="height: 20px" __designer:mapid="4b">Weeks as of Payrol</td>
            <td style="height: 20px; width: 674px;" __designer:mapid="4c">
                <asp:TextBox ID="TXTPRWeeks" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr __designer:mapid="4a">
            <td style="height: 20px" __designer:mapid="4b">&nbsp;</td>
            <td style="height: 20px; width: 674px;" __designer:mapid="4c">&nbsp;</td>
        </tr>
        <tr __designer:mapid="4d">
            <td style="height: 20px" __designer:mapid="4e">
                <br __designer:mapid="4f" />
                <br __designer:mapid="50" /></td>
            <td style="height: 20px; width: 674px;" __designer:mapid="51">
                <asp:Button ID="Button1" runat="server" BackColor="#3366CC" BorderColor="#003399" BorderStyle="Solid" Font-Bold="True" ForeColor="White" Height="36px" Text="Calculate &gt;&gt;" Width="143px" />
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                </asp:UpdateProgress>
            </td>
        </tr>
        <tr __designer:mapid="55">
            <td style="height: 20px" colspan="2" __designer:mapid="56">
                <asp:UpdateProgress ID="UpdateProgress2" runat="server">
                    <ProgressTemplate>
                        <asp:Image ID="Image1" runat="server" Height="52px" ImageUrl="~/Image/SkinnySeveralAsianlion.gif" Width="56px" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
        <tr __designer:mapid="5a">
            <td colspan="2" style="height: 20px" __designer:mapid="5b">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" Width="100%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:BoundField DataField="stc_ck" HeaderText="Store" SortExpression="stc_ck" />
                        <asp:BoundField DataField="st_nam" HeaderText="Store Name" SortExpression="st_nam" />
                        <asp:BoundField DataField="emp_ck" HeaderText="Employee" SortExpression="emp_ck" />
                        <asp:BoundField DataField="nam_em" HeaderText="Name" SortExpression="nam_em" />
                        <asp:BoundField DataField="TotalHours" HeaderText="Total Hours" ReadOnly="True" SortExpression="TotalHours" />
                        <asp:BoundField DataField="WeekNumber" HeaderText="Week Number" SortExpression="WeekNumber" Visible="False" />
                        <asp:BoundField DataField="OverTime" HeaderText="Over Time" ReadOnly="True" SortExpression="OverTime" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:KWIConnectionString %>" SelectCommand="SELECT stc_ck, st_nam, emp_ck, nam_em, SUM(TotalHours) AS TotalHours, WeekNumber, CONVERT (DECIMAL(10 , 2), (CASE WHEN SUM(totalHours) &gt; 40 THEN SUM(totalhours) - 40 ELSE 0 END)) AS OverTime FROM View_EmpTimeAttenWithOT WHERE (ym_ck BETWEEN @from AND @to ) GROUP BY stc_ck, st_nam, emp_ck, nam_em, WeekNumber HAVING (emp_ck = @emp_ck) ORDER BY WeekNumber">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="TXTDateFrom" Name="from" PropertyName="Text" />
                        <asp:ControlParameter ControlID="TXTDateTo" Name="to" PropertyName="Text" />
                        <asp:ControlParameter ControlID="DropDownList2" Name="emp_ck" PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>
