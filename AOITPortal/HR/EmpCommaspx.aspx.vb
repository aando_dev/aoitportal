﻿Public Class EmpCommaspx
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If GridView1.Rows.Count > 0 Then
            Dim AvgH, OT As Double
            Dim ro As GridViewRow
            Dim TotalHours As Double

            For Each ro In GridView1.Rows
                Try
                    TotalHours += CDec(ro.Cells(4).Text)
                    OT += CDec(ro.Cells(6).Text)
                Catch ex As Exception

                End Try
            Next
            AvgH = TotalHours / CInt(TXTPRWeeks.Text)
            Dim Comm, AddCom, IncreaseRate, NewIncrease, AddWages As Decimal
            Comm = TXTCom.Text
            AddCom = Comm / CDec(TXTWeeks.Text)
            IncreaseRate = AddCom / AvgH
            NewIncrease = IncreaseRate * 1.5
            AddWages = NewIncrease * OT
            TXTAddCom.Text = AddCom
            TXTIncinRegRate.Text = IncreaseRate
            TXTNewIncOTRate.Text = NewIncrease
            TXTAddWages.Text = AddWages

        End If
    End Sub
End Class