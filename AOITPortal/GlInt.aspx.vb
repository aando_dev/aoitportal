﻿
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Data
Public Class GlInt
    Inherits System.Web.UI.Page
    Dim dTotalDebit As Decimal = 0
    Dim dTotalCredit As Decimal = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ScreenID As Integer = 2
        Try
            Dim dapUA As New AOWebPortaDSTableAdapters.UserAccessTableAdapter
            Dim ds As New AOWebPortaDS.UserAccessDataTable
            dapUA.FillByUserAndScreen(ds, User.Identity.Name, ScreenID)
            If ds.Rows.Count = 0 Then
                Response.Redirect("./Error.aspx")
            Else
                Dim dapLog As New AOWebPortaDSTableAdapters.LoginLogsTableAdapter
                dapLog.InsertLogin(User.Identity.Name, ScreenID)
            End If
        Catch ex As Exception
            Response.Redirect("./Error.aspx")
        End Try

    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click


        GridView1.DataSourceID = SqlDataSource1.ID
        GridView1.DataBind()
    End Sub

    Protected Sub SqlDataSource1_Selecting(sender As Object, e As SqlDataSourceSelectingEventArgs) Handles SqlDataSource1.Selecting
        e.Command.CommandTimeout = 300

    End Sub

    Protected Sub GridView1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView1.SelectedIndexChanged

    End Sub

    Private Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            dTotalDebit += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Debit"))
            dTotalCredit += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Credit"))
        End If
        If e.Row.RowType = DataControlRowType.Footer Then
            e.Row.Cells(1).Text = "Totals:"
            If dTotalDebit < 0 Then
                e.Row.Cells(4).Text = (-1 * dTotalDebit).ToString("c")
                e.Row.Cells(5).Text = (-1 * dTotalCredit).ToString("c")
            Else
                e.Row.Cells(4).Text = dTotalDebit.ToString("c")
                e.Row.Cells(5).Text = dTotalCredit.ToString("c")
            End If

            ' e.Row.Cells(1).HorizontalAlign = HorizontalAlign.Right
            ' e.Row.Cells(2).HorizontalAlign = HorizontalAlign.Right
            e.Row.Font.Bold = True

        End If


    End Sub

    Protected Sub ImageButton1_Click(sender As Object, e As ImageClickEventArgs) Handles ImageButton1.Click
        If GridView1.DataSourceID = SqlDataSource1.ID Then
            If GridView1.Rows.Count > 0 Then
                Dim csv As String = String.Empty

                'Download the CSV file.
                Response.Clear()
                Response.Buffer = True
                Response.AddHeader("content-disposition", "attachment;filename=" + DropDownList2.SelectedValue.ToString + ".csv")
                Response.Charset = ""
                Response.ContentType = "application/text"
                Dim sBuilder As StringBuilder = New System.Text.StringBuilder()
                '  For index As Integer = 0 To GridView1.Columns.Count - 3
                ' sBuilder.Append(GridView1.Columns(index).HeaderText + ","c)
                ' Next
                ' sBuilder.AppendLine()

                For i As Integer = 0 To GridView1.Rows.Count - 1

                    For k As Integer = 0 To GridView1.HeaderRow.Cells.Count - 3
                        If k = 0 Then
                            ' sBuilder.Append("Level1" + "," + GridView1.Rows(i).Cells(k).Text.Replace(",", "") + ",")
                            sBuilder.Append("Level1" + "," + TXTDateTo.Text + "," + GridView1.Rows(i).Cells(k).Text.Replace(",", "") + ",")

                        Else
                            sBuilder.Append(GridView1.Rows(i).Cells(k).Text.Replace(",", "") + ",")
                        End If

                        'sBuilder.Append(GridView1.Rows(i).Cells(k).Text + ","c)
                    Next
                    sBuilder.AppendLine()
                Next
                Response.Write(sBuilder.ToString)
                '  Response.Output.Write(sBuilder.ToString)
                Response.Flush()

                Response.End()

            End If
        Else
            If GridView1.Rows.Count > 0 Then
                Dim csv As String = String.Empty

                'Download the CSV file.
                Response.Clear()
                Response.Buffer = True
                Response.AddHeader("content-disposition", "attachment;filename=" + DropDownList2.SelectedValue.ToString + ".csv")
                Response.Charset = ""
                Response.ContentType = "application/text"
                Dim sBuilder As StringBuilder = New System.Text.StringBuilder()

                sBuilder.AppendLine()
                For i As Integer = 0 To GridView1.Rows.Count - 1
                    '  For k As Integer = 0 To GridView1.HeaderRow.Cells.Count - 1
                    'sBuilder.Append(GridView1.Rows(i).Cells(k).Text + ","c)
                    'Next
                    For index As Integer = 0 To 8
                        sBuilder.Append(GridView1.Rows(i).Cells(index).Text.ToString.TrimEnd + ","c)
                    Next
                    sBuilder.AppendLine()
                    Next
                    Response.Write(sBuilder.ToString)

                Response.Flush()

                Response.End()

            End If
        End If


    End Sub

    Private Sub GridView1_DataBound(sender As Object, e As EventArgs) Handles GridView1.DataBound
        Dim ro As GridViewRow
        For Each ro In GridView1.Rows
            Try
                ' If ro.Cells(1).Text = "40100" Then
                'ro.Cells(5).Text = dTotalDebit + dTotalCredit
                'ro.Cells(6).Text = DropDownList2.SelectedValue + ro.Cells(6).Text
                ' End If

                ro.Cells(0).Text = "D000"

                If ro.Cells(1).Text = "10000" Then

                    ro.Cells(6).Text = DropDownList2.SelectedValue + ro.Cells(6).Text
                End If
                If ro.Cells(1).Text = "12010" Then

                    ro.Cells(6).Text = DropDownList2.SelectedValue + ro.Cells(6).Text
                End If
                If CDec(ro.Cells(4).Text) < 0 Then
                    ro.Cells(4).Text = ro.Cells(4).Text * -1
                End If
                If CDec(ro.Cells(5).Text) < 0 Then
                    ro.Cells(5).Text = ro.Cells(5).Text * -1
                End If
                ro.Cells(8).Text = TXTDateTo.Text
            Catch ex As Exception

            End Try

        Next
    End Sub

    Protected Sub TXTDateFrom_TextChanged(sender As Object, e As EventArgs) Handles TXTDateFrom.TextChanged
        Try
            '  DropDownList2.SelectedValue = CInt(Left(TXTDateFrom.Text, 3))
        Catch ex As Exception


        End Try
    End Sub

    Protected Sub TXTDateTo_TextChanged(sender As Object, e As EventArgs) Handles TXTDateTo.TextChanged

    End Sub

    Protected Sub ImageButton2_Click(sender As Object, e As ImageClickEventArgs) Handles ImageButton2.Click
        '   SqlDataSource1.Delete()
        '  Session.Add("TrxSess", 0)
        ' SqlDataSource1.InsertParameters(0).DefaultValue = Session.Item("TrxSess").ToString
        '' SqlDataSource1.InsertParameters(1).DefaultValue = 1
        ' SqlDataSource1.InsertParameters(14).DefaultValue = TXTPrepBy.Text
        'SqlDataSource1.InsertParameters(13).DefaultValue = DropDownList2.SelectedIndex + 1
        '  SqlDataSource1.InsertParameters(12).DefaultValue = DropDownList2.SelectedValue.ToString + "-" + Date.Now.Year.ToString
        '   SqlDataSource1.InsertParameters(11).DefaultValue = TXTBatch.Text
        ' Dim ro As GridViewRow
        ' For Each ro In GridView1.Rows
        'Try
        '  SqlDataSource1.InsertParameters(2).DefaultValue = ro.Cells(0).Text
        '  SqlDataSource1.InsertParameters(3).DefaultValue = ro.Cells(1).Text
        '  SqlDataSource1.InsertParameters(4).DefaultValue = ro.Cells(2).Text
        '  SqlDataSource1.InsertParameters(5).DefaultValue = ro.Cells(3).Text
        '  SqlDataSource1.InsertParameters(6).DefaultValue = ro.Cells(8).Text
        ' SqlDataSource1.InsertParameters(7).DefaultValue = ro.Cells(4).Text
        ' SqlDataSource1.InsertParameters(8).DefaultValue = ro.Cells(5).Text
        ' SqlDataSource1.InsertParameters(9).DefaultValue = ro.Cells(6).Text
        ' SqlDataSource1.InsertParameters(10).DefaultValue = ro.Cells(7).Text
        ' SqlDataSource1.Insert()
        '  Catch ex As Exception

        ' End Try
        ' Next
        '   Response.Redirect("./about.aspx")
    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            Dim dap As New AOWebPortaDSTableAdapters.COGS_transTBLTableAdapter

            dap.FillCOGS_transTBL()

            GridView1.DataSourceID = COGSDS.ID
            GridView1.DataBind()
        Catch ex As Exception

        End Try

    End Sub
End Class