﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AdyenPaymentEraze.aspx.vb" Inherits="AOITPortal.AdyenPaymentEraze" %>
<%@ Register src="../Counter.ascx" tagname="counter" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="nav-justified">
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 40px; font-size: xx-large;" class="text-left"><strong>Adyen CCPA process</strong></td>
                </tr>
                <tr>
                    <td class="text-left" style="height: 16px; font-size: xx-large;">
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:AODEVConnectionString %>" SelectCommand="SELECT * FROM [Adyen_Trx]" InsertCommand="INSERT INTO Adyen_Trx(Username, PSPreference, Result, DT) VALUES (@Username, @PSPreference, @Result, GETDATE())">
                            <InsertParameters>
                                <asp:Parameter Name="Username" />
                                <asp:Parameter Name="PSPreference" />
                                <asp:Parameter Name="Result" />
                            </InsertParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td style="height: 40px">
                        <asp:TextBox ID="TXT_PSPRef" runat="server" Height="32px" Width="212px"></asp:TextBox>
                        <ajaxToolkit:TextBoxWatermarkExtender ID="TXT_PSPRef_TextBoxWatermarkExtender" runat="server" BehaviorID="TXT_OrderID_TextBoxWatermarkExtender" TargetControlID="TXT_PSPRef" WatermarkText="Enter the PSPReference" />
                        <asp:Button ID="BTN_PROCESS" runat="server" BackColor="#4D4F6F" BorderStyle="Solid" Font-Size="Small" ForeColor="White" Height="40px" Text="PROCESS" Width="103px" Font-Bold="True" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter the PSPReference" ForeColor="Red" ControlToValidate="TXT_PSPRef"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td><strong><span style="font-size: 16pt; color: #666666">&nbsp;</span></strong></div><asp:Label ID="Label1" runat="server" ForeColor="#666666" Font-Size="16pt" style="font-size: x-large"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <asp:Image ID="Image1" runat="server" Height="52px" ImageUrl="~/Image/SkinnySeveralAsianlion.gif" Width="56px" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DetailsView ID="DetailsView1" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" Height="50px" Width="125px">
                            <AlternatingRowStyle BackColor="White" />
                            <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
                            <EditRowStyle BackColor="#7C6F57" />
                            <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#E3EAEB" />
                        </asp:DetailsView>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
