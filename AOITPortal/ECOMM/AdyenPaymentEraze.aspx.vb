﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.IO
Imports System.Data
Imports System.Xml
Imports System.Net
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports System.Xml.Schema
Imports System.Data.SqlTypes
Public Class AdyenPaymentEraze
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ScreenID As Integer = 10
        Try
            Dim dapUA As New AOWebPortaDSTableAdapters.UserAccessTableAdapter
            Dim ds As New AOWebPortaDS.UserAccessDataTable
            dapUA.FillByUserAndScreen(ds, User.Identity.Name, ScreenID)
            If ds.Rows.Count = 0 Then
                Response.Redirect("../Error.aspx")
            Else
                Dim dapLog As New AOWebPortaDSTableAdapters.LoginLogsTableAdapter
                dapLog.InsertLogin(User.Identity.Name, ScreenID)
            End If
        Catch ex As Exception
            Response.Redirect("../Error.aspx")
        End Try
        Me.Form.DefaultButton = BTN_PROCESS.UniqueID
        'Label1.Text = ""


    End Sub

    Protected Sub BTN_PROCESS_Click(sender As Object, e As EventArgs) Handles BTN_PROCESS.Click
        Label1.Text = ""
        Try

            Dim myConn As New SqlConnection
            Dim TopCmd As New SqlCommand
            Dim APIKeyCmd As New SqlCommand
            Dim APIURLCmd As New SqlCommand
            myConn.ConnectionString = "Data Source=aobcdb1;Initial Catalog=AOViews;Persist Security Info=True;User ID=sa;Password=bcadmin"
            TopCmd.CommandText = "SELECT Test_Production from AO_Dev_Apps_Variables where App_Code='Adyen' and Variable_Name='ToP'"
            TopCmd.Connection = myConn
            APIKeyCmd.Connection = myConn
            APIURLCmd.Connection = myConn
            myConn.Open()

            Dim APIKey As String
            Dim APIURL As String
            Dim TOP As Integer
            TOP = TopCmd.ExecuteScalar()
            APIKeyCmd.CommandText = "SELECT  Variable_Value from AO_Dev_Apps_Variables where App_Code='Adyen' and Variable_Name='API_Key' and Test_Production=" + TOP.ToString()
            APIURLCmd.CommandText = "SELECT  Variable_Value from AO_Dev_Apps_Variables where App_Code='Adyen' and Variable_Name='API_URL' and Test_Production=" + TOP.ToString()


            APIKey = APIKeyCmd.ExecuteScalar()
            APIURL = APIURLCmd.ExecuteScalar()
            myConn.Close()

            Dim postData As String = "{""merchantAccount"":""PeachesAndPantsLLCECOM"",""pspReference"":""" + TXT_PSPRef.Text + """, ""forceErasure"": true}"
            Dim tempCookies As New CookieContainer
            Dim encoding As New UTF8Encoding
            Dim byteData As Byte() = encoding.GetBytes(postData)

            'Dim postReq As HttpWebRequest = DirectCast(WebRequest.Create("https://ca-test.adyen.com/ca/services/DataProtectionService/v1/requestSubjectErasure"), HttpWebRequest)
            Dim postReq As HttpWebRequest = DirectCast(WebRequest.Create(APIURL), HttpWebRequest)

            postReq.Method = "POST"
            'postReq.KeepAlive = True
            postReq.ContentType = "application/json"
            '  postReq.Headers.Add("content-type", "application/json")
            '  postReq.Headers.Add("x-API-key", "AQEthmfuXNWTK0Qc+iSAl2U7r+GIaYVJPZJETHZ66lstffdLHI3VHl51r8soGmsoEMFdWw2+5HzctViMSCJMYAc=-svGpMwvs/7VW5mt4UGh5ZjUXL2IgHW5tMKqaORnaYwg=-94f+5M~vX^>U6?7u")
            postReq.Headers.Add("x-API-key", APIKey)

            postReq.ContentLength = byteData.Length



            Dim postreqstream As Stream = postReq.GetRequestStream()
            postreqstream.Write(byteData, 0, byteData.Length)
            postreqstream.Close()
            Dim postresponse As HttpWebResponse

            postresponse = DirectCast(postReq.GetResponse(), HttpWebResponse)

            Dim postreqreader As New StreamReader(postresponse.GetResponseStream())

            Dim thepage As String = postreqreader.ReadToEnd


            Label1.Text = thepage
            SqlDataSource1.InsertParameters.Item(0).DefaultValue = User.Identity.Name
            SqlDataSource1.InsertParameters.Item(1).DefaultValue = TXT_PSPRef.Text
            SqlDataSource1.InsertParameters.Item(2).DefaultValue = thepage
            SqlDataSource1.Insert()

        Catch ex As Exception
            Label1.Text = ex.Message
        End Try


    End Sub
End Class