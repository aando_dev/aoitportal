﻿Imports System.DirectoryServices.AccountManagement
Public Class RuturnsGiftCard
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ScreenID As Integer = 4
        Try
            Dim dapUA As New AOWebPortaDSTableAdapters.UserAccessTableAdapter
            Dim ds As New AOWebPortaDS.UserAccessDataTable
            dapUA.FillByUserAndScreen(ds, User.Identity.Name, ScreenID)
            If ds.Rows.Count = 0 Then
                Response.Redirect("../Error.aspx")
            Else
                Dim dapLog As New AOWebPortaDSTableAdapters.LoginLogsTableAdapter
                dapLog.InsertLogin(User.Identity.Name, ScreenID)
            End If
        Catch ex As Exception
            Response.Redirect("../Error.aspx")
        End Try

    End Sub


    Private Sub GridView1_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles GridView1.RowUpdating
        Label1.Visible = False
        ' Dim row As GridViewRow = GridView1.Rows(e.RowIndex)
        ' Dim TID As Integer = Convert.ToInt32(GridView1.DataKeys(e.RowIndex).Values(0))
        ' Dim rownum As Integer = Convert.ToInt32(GridView1.DataKeys(e.RowIndex).Values(1))
        ' Dim Qntyold As String = row.Cells(15).Text
        ' Dim ponum As String = row.Cells(4).Text
        ' Dim lineNum As String = row.Cells(7).Text
        ' Dim Qnty As String = TryCast(row.Cells(17).Controls(0), TextBox).Text
        ' Try
        'If CInt(Qnty) <= Qntyold Then
        'Dim yesno As String = TryCast(row.Cells(16).Controls(0), CheckBox).Checked
        'Try
        'If yesno = "True" Then
        'SqlDataSource1.InsertParameters(1).DefaultValue = 1
        'Else
        'SqlDataSource1.InsertParameters(1).DefaultValue = 0
        'End If
        'SqlDataSource1.InsertParameters(3).DefaultValue = Qnty
        'SqlDataSource1.InsertParameters(0).DefaultValue = rownum
        'SqlDataSource1.InsertParameters(2).DefaultValue = User.Identity.Name
        'SqlDataSource1.InsertParameters(4).DefaultValue = Qntyold
        'SqlDataSource1.InsertParameters(5).DefaultValue = ponum
        'SqlDataSource1.InsertParameters(6).DefaultValue = lineNum
        'SqlDataSource1.Insert()
        'GridView2.DataBind()
        'Catch ex As Exception
        'Label1.Visible = True
        'End Try
        'Else
        'Label1.Visible = True
        'Exit Sub
        'End If
        'Catch ex As Exception
        'Label1.Visible = True
        'Exit Sub
        'End Try



    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Label1.Visible = False

    End Sub

    Private Sub GridView1_DataBound(sender As Object, e As EventArgs) Handles GridView1.DataBound

        If GridView1.Rows.Count > 0 Then
            Button2.Visible = True
        Else
            Button2.Visible = False
        End If
    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Label1.Visible = False
        Dim rowval As GridViewRow
        For Each rowval In GridView1.Rows
            Dim chk As CheckBox = rowval.Cells(10).Controls.Item(1).FindControl("CheckBox1")
            If chk.Checked = True Then
                Try
                    SqlDataSource2.InsertParameters(0).DefaultValue = rowval.Cells(0).Text
                    SqlDataSource2.InsertParameters(1).DefaultValue = User.Identity.Name
                    SqlDataSource2.InsertParameters(2).DefaultValue = rowval.Cells(3).Text
                    SqlDataSource2.InsertParameters(3).DefaultValue = rowval.Cells(1).Text
                    SqlDataSource2.InsertParameters(4).DefaultValue = rowval.Cells(4).Text
                    SqlDataSource2.Insert()

                Catch ex As Exception

                End Try

            End If
        Next
        SqlDataSource1.DataBind()
        GridView1.DataBind()
    End Sub
End Class