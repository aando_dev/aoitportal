﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ReturnGiftCard.aspx.vb" Inherits="AOITPortal.RuturnsGiftCard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="nav-justified">
                <tr>
                    <td style="height: 67px"></td>
                    <td style="height: 67px">
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="height: 32px"><strong>PO. Number </strong></td>
                    <td style="height: 32px">
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                        <asp:Label ID="Label1" runat="server" Text="Please Check the Quantity " Font-Bold="True" ForeColor="Red" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="height: 22px" colspan="2">
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <asp:Image ID="Image1" runat="server" Height="52px" ImageUrl="~/Image/SkinnySeveralAsianlion.gif" Width="56px" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="height: 22px">&nbsp;</td>
                    <td style="height: 22px">
                        <asp:Button ID="Button1" runat="server" style="margin-top: 0" Text="Find PO" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="text-center">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" DataKeyNames="work_order_sku_id" DataMember="DefaultView" DataSourceID="SqlDataSource1" EnablePersistedSelection="True" EnableSortingAndPagingCallbacks="True" ForeColor="Black" Width="100%" AllowSorting="True" CaptionAlign="Top" CellSpacing="2">
                                <AlternatingRowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <Columns>
                                    <asp:BoundField DataField="work_order_sku_id" HeaderText="rowid" ReadOnly="True" SortExpression="work_order_sku_id" />
                                    <asp:BoundField DataField="RA_Number" HeaderText="RA No." SortExpression="RA_Number" ReadOnly="True" >
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="RA_Date" DataFormatString="{0:d}" HeaderText="Date" SortExpression="RA_Date" ReadOnly="True" >
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PO_Number" HeaderText="PO No." SortExpression="PO_Number" ReadOnly="True" />
                                    <asp:BoundField DataField="UPC" HeaderText="UPC" SortExpression="UPC" ReadOnly="True" />
                                    <asp:BoundField DataField="updated_date" HeaderText="Update Date" SortExpression="updated_date" ReadOnly="True" />
                                    <asp:BoundField DataField="tracking_number" HeaderText="Tracking Number" SortExpression="tracking_number" >
                                    </asp:BoundField>
                                    <asp:BoundField DataField="return_reason" HeaderText="Return Reason" SortExpression="return_reason" />
                                    <asp:BoundField DataField="export_status" HeaderText="Export Status" SortExpression="export_status" />
                                    <asp:HyperLinkField DataNavigateUrlFields="label_url" DataTextField="RA_Number" HeaderText="Label Link" Target="_blank" />
                                    <asp:TemplateField HeaderText="Gift Card">
                <ItemTemplate>
                    <asp:CheckBox ID="CheckBox1" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
                                </Columns>
                                <EditRowStyle Width="30px" />
                                <FooterStyle BackColor="#CCCCCC" />
                                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                <RowStyle BackColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#808080" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#383838" />
                            </asp:GridView>
                        </div>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ACDCConnectionString %>" SelectCommand="SELECT work_order_sku_id, PO_Number, UPC, RA_Number, RA_Date, return_status_id, return_status_name, updated_date, tracking_number, label_url, return_sku_id, sku_id, return_reason, exchange, credit_amount, return_sku_status_id, export_status_id, export_status, work_order_id, cost, unit_price, original_unit_price, po_shipment_sku_id, io FROM View_ACDC_Return WHERE (PO_Number = @PO_Number) AND (work_order_sku_id NOT IN (SELECT RowID FROM AOWebPortal.dbo.ECM_GiftCardYesNo)) ORDER BY RA_Number, UPC" UpdateCommand="UPDATE ECM_GiftCardYesNo SET userID=UserID where TID=@TID" ProviderName="<%$ ConnectionStrings:ACDCConnectionString.ProviderName %>">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="TextBox1" Name="PO_Number" PropertyName="Text" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:ControlParameter ControlID="GridView1" Name="TID" PropertyName="SelectedValue" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                        <asp:Button ID="Button2" runat="server" style="margin-top: 0" Text="Save" Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 20px">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 20px">Update Log:</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSource2" ForeColor="#333333" GridLines="None" Width="100%">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            <Columns>
                                <asp:BoundField DataField="PONumber" HeaderText="PO Number" SortExpression="PONumber" />
                                <asp:BoundField DataField="RANumber" HeaderText="RA Number" SortExpression="RANumber" />
                                <asp:BoundField DataField="UPC" HeaderText="UPC" SortExpression="UPC" />
                                <asp:BoundField DataField="UserID" HeaderText="User" SortExpression="UserID" />
                                <asp:BoundField DataField="TransDateTime" HeaderText="Update Date and Time" SortExpression="TransDateTime" />
                                <asp:BoundField DataField="QTY_Gift" HeaderText="QTY" SortExpression="QTY_Gift" />
                            </Columns>
                            <EditRowStyle BackColor="#999999" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:AOWebPortalConnectionString %>" SelectCommand="SELECT TID, RowID, UserID, TransDateTime, QTY_Gift, PONumber, RANumber, UPC FROM ECM_GiftCardYesNo WHERE (PONumber = @PONumber)" InsertCommand="INSERT INTO ECM_GiftCardYesNo(RowID, UserID, TransDateTime, PONumber, RANumber, QTY_Gift, UPC) VALUES (@Rowid, @userID, GETDATE(), @PONumber, @RANumber, 1, @UPC)">
                            <InsertParameters>
                                <asp:Parameter Name="Rowid" />
                                <asp:Parameter Name="userID" />
                                <asp:Parameter Name="PONumber" />
                                <asp:Parameter Name="RANumber" />
                                <asp:Parameter Name="UPC" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:ControlParameter ControlID="TextBox1" Name="PONumber" PropertyName="Text" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
