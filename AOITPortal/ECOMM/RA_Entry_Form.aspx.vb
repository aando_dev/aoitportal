﻿Imports System.Data.SqlClient
Public Class RA_Entry_Form
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim ScreenID As Integer = 8
        Try
            Dim dapUA As New AOWebPortaDSTableAdapters.UserAccessTableAdapter
            Dim ds As New AOWebPortaDS.UserAccessDataTable
            dapUA.FillByUserAndScreen(ds, User.Identity.Name, ScreenID)
            If ds.Rows.Count = 0 Then
                Response.Redirect("../Error.aspx")
            Else
                Dim dapLog As New AOWebPortaDSTableAdapters.LoginLogsTableAdapter
                dapLog.InsertLogin(User.Identity.Name, ScreenID)
            End If
        Catch ex As Exception
            Response.Redirect("../Error.aspx")
        End Try
        Me.Form.DefaultButton = BTN_Search.UniqueID
        Label1.Text = ""
        BTN_Process.Visible = False
        LBL_Total.Text = ""

    End Sub



    Protected Sub BTN_Search_Click(sender As Object, e As EventArgs) Handles BTN_Search.Click
        Label1.Text = ""
        BTN_Process.Visible = False
        LBL_Total.Text = ""

        If IsNumeric(TXT_OrderID.Text) = False Then
            ' MsgBox("invalid order ID", vbYes, "Error")
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "alertMessage", "alert('Invalid order ID')", True)

            '  LBL_Error.Text = ("invalid order ID")
            TXT_OrderID.Text = ""
            TXT_OrderID.Focus()
        End If

        '  Session.Add("totalqnty", "0")
        ' If Me.ListView2.Items.Count > 0 Then
        'Label1.Text = "ORDER # " + TXT_OrderID.Text
        'BTN_Process.Visible = True
        'LBL_Error.Text = ""
        'LBL_Total.Text = ""
        'Else
        'LBL_Error.Text = "Order not Found"
        'End If

    End Sub

    Protected Sub BTN_Process_Click(sender As Object, e As EventArgs) Handles BTN_Process.Click
        ' LBL_Error.Text = ""
        Dim qntctl As New TextBox
        Dim currqntctl As New Label

        For Each item As ListViewItem In Me.ListView2.Items
            Dim reasonctl As New DropDownList
            reasonctl = item.Controls(15)
            Dim rescode As String
            rescode = reasonctl.SelectedValue
            Dim checkctl As New CheckBox
            Dim Refundyn As String
            qntctl = item.Controls(19).Controls(3)
            currqntctl = item.Controls(17)
            checkctl = item.Controls(21)
            If checkctl.Checked = True Then
                Refundyn = "Y"
            Else
                Refundyn = "N"
            End If
            Dim qnty As String = qntctl.Text
            Dim currqnty As String = currqntctl.Text

            If CInt(qnty) > 0 Then
                If CInt(qnty) = CInt(currqnty) Then
                    If rescode = "0" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "alertMessage", "alert('Please select the return reason for Order Line no." + (item.DisplayIndex + 1).ToString + "')", True)
                        Exit Sub
                    End If

                ElseIf CInt(qnty) > CInt(currqnty) Then
                    If Refundyn = "N" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "alertMessage", "alert('The quantity to be returned is greater than the ordered quantity, Order Line no." + (item.DisplayIndex + 1).ToString + "')", True)
                        Exit Sub
                    Else Refundyn = "Y"
                        If rescode = "0" Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "alertMessage", "alert('Please select the return reason for Order Line no." + (item.DisplayIndex + 1).ToString + "')", True)
                            Exit Sub
                        End If
                    End If
                End If
            ElseIf CInt(qnty) = 0 Then
                If rescode <> "0" Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "alertMessage", "alert('Please select the Qty. to be Returned for Order Line no." + (item.DisplayIndex + 1).ToString + "')", True)
                    Exit Sub
                End If

                If Refundyn = "Y" Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "alertMessage", "alert('Please select the Qty. to be Returned for Order Line no." + (item.DisplayIndex + 1).ToString + "')", True)
                    Exit Sub

                End If

            End If


        Next

        Submit_Process()

        'BTN_Process.Enabled = False
    End Sub

    Private Sub ListView2_PreRender(sender As Object, e As EventArgs) Handles ListView2.PreRender

        Try
            If Me.ListView2.Items.Count > 0 Then
                Dim totalqnty = 0
                Label1.Text = "ORDER # " + TXT_OrderID.Text
                BTN_Process.Visible = True


                For Each item As ListViewItem In Me.ListView2.Items
                    Dim qntctl As New TextBox
                    qntctl = item.Controls(19).Controls(3)
                    totalqnty += CInt(qntctl.Text)
                Next
                LBL_Total.Text = totalqnty.ToString() + " Total Units to be Returned"
                ' Session("totalqnty") = totalqnty.ToString()
            Else
                Label1.Text = ""
                BTN_Process.Visible = False
                LBL_Total.Text = ""

            End If

        Catch ex As Exception
            Label1.Text = ""
            BTN_Process.Visible = False
            LBL_Total.Text = ""
            Exit Sub
        End Try
    End Sub
    Private Sub Submit_Process()
        Dim con As New SqlConnection
        con.ConnectionString = "Data Source=aoaccessreports;Initial Catalog=beowulf;Persist Security Info=True;User ID=sa;Password=65656565"

        For Each item As ListViewItem In Me.ListView2.Items
            Dim qntctl As New TextBox
            Dim currqntctl As New Label
            Dim orderlineCtl As New Label
            Dim StyleCtl As New Label
            Dim StylenameCtl As New Label
            Dim colorCrl As New Label
            Dim ColorNameCtl As New Label
            Dim sizeCtl As New Label
            Dim upcCtl As New Label
            Dim reasonctl As New DropDownList
            Dim checkctl As New CheckBox
            Dim orderline As String
            Dim style As String
            Dim stylename As String
            Dim color As String
            Dim colorname As String
            Dim size As String
            Dim upc As String
            Dim rescode As String
            Dim reasname As String
            Dim Refundyn As String
            orderlineCtl = item.Controls(1)
            orderline = orderlineCtl.Text.Trim
            StyleCtl = item.Controls(3)
            style = StyleCtl.Text.Trim
            StylenameCtl = item.Controls(5)
            stylename = StylenameCtl.Text.Trim
            colorCrl = item.Controls(7)
            color = colorCrl.Text.Trim
            ColorNameCtl = item.Controls(9)
            colorname = ColorNameCtl.Text.Trim
            sizeCtl = item.Controls(11)
            size = sizeCtl.Text.Trim
            upcCtl = item.Controls(13)
            upc = upcCtl.Text.Trim
            reasonctl = item.Controls(15)
            rescode = reasonctl.SelectedValue
            reasname = reasonctl.SelectedItem.Text
            qntctl = item.Controls(19).Controls(3)
            currqntctl = item.Controls(17)
            checkctl = item.Controls(21)
            If checkctl.Checked = True Then
                Refundyn = "Y"
            Else
                Refundyn = "N"
            End If
            Dim qnty As String = qntctl.Text
            Dim currqnty As String = currqntctl.Text
            Dim checkstring As String = "select count(orderid) as olines from View_EC_MAN_RA_Form where OrderId='" + TXT_OrderID.Text + "' and OrderLine='" + orderline + "' and upc='" + upc + "'"
            Dim com As New SqlCommand(checkstring, con)
            con.Open()
            Dim ckeckoline = com.ExecuteScalar
            con.Close()

            If CInt(qnty) > 0 And ckeckoline > 0 Then
                Try
                    SqlOrderDet.InsertParameters("OrderId").DefaultValue = TXT_OrderID.Text
                    SqlOrderDet.InsertParameters("UserId").DefaultValue = User.Identity.Name
                    SqlOrderDet.InsertParameters("Rsn_Desc").DefaultValue = reasname
                    SqlOrderDet.InsertParameters("Rsn_code").DefaultValue = rescode
                    SqlOrderDet.InsertParameters("qty_retn").DefaultValue = qnty
                    SqlOrderDet.InsertParameters("upc").DefaultValue = upc
                    SqlOrderDet.InsertParameters("size").DefaultValue = size
                    SqlOrderDet.InsertParameters("color_name").DefaultValue = colorname
                    SqlOrderDet.InsertParameters("Color").DefaultValue = color
                    SqlOrderDet.InsertParameters("style_name").DefaultValue = stylename
                    SqlOrderDet.InsertParameters("Style").DefaultValue = style
                    SqlOrderDet.InsertParameters("OrderLine").DefaultValue = orderline
                    SqlOrderDet.InsertParameters("NO_Refund").DefaultValue = Refundyn
                    SqlOrderDet.Insert()
                Catch ex As Exception
                    Exit Sub
                End Try
            ElseIf CInt(qnty) > 0 And ckeckoline = 0 Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "alertMessage", "alert('Unable to process the RA,invalid Order number!')", True)
                Label1.Text = ""
                BTN_Process.Visible = False
                LBL_Total.Text = ""
                Exit Sub
            End If

        Next

        Session.Add("totalqnty", 1)
        Response.Redirect("RA_Completed.aspx")

    End Sub


End Class