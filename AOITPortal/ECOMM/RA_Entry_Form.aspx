﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="RA_Entry_Form.aspx.vb" Inherits="AOITPortal.RA_Entry_Form" %>
<%@ Register Src="~/Counter.ascx" TagName="counter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="nav-justified">
                <tr>
                    <td>
                       
                        <asp:SqlDataSource ID="SqlReason" runat="server" ConnectionString="<%$ ConnectionStrings:beowulfConnectionString2 %>" SelectCommand="select '0' as rsn_code , '' as rsn_desc 
union all
select rsn_code, rsn_desc 
from aobcdb1.dataano.dbo.zznresnr where LEFT(ltrim(rsn_desc), 2) = 'EC'
"></asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td style="height: 40px; font-size: xx-large;" class="text-left">
                        <strong>ECOM RA ENTRY</strong></td>
                </tr>
                <tr>
                    <td class="text-left" style="height: 16px; font-size: xx-large;"></td>
                </tr>
                <tr>
                    <td style="height: 40px">
                        <asp:TextBox ID="TXT_OrderID" runat="server" Height="32px" Width="212px"></asp:TextBox>
                        <ajaxToolkit:TextBoxWatermarkExtender ID="TXT_OrderID_TextBoxWatermarkExtender" runat="server" BehaviorID="TXT_OrderID_TextBoxWatermarkExtender" TargetControlID="TXT_OrderID" WatermarkText="Enter ORDER NUMBER" />
                        <asp:Button ID="BTN_Search" runat="server" BackColor="#4D4F6F" BorderStyle="Solid" Font-Size="Small" ForeColor="White" Height="40px" Text="SEARCH" Width="140px" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><strong><span style="font-size: 16pt; color: #666666"> &nbsp;</span></strong></div><asp:Label ID="Label1" runat="server" ForeColor="#666666" Font-Size="16pt" style="font-size: x-large"></asp:Label>
                        <asp:SqlDataSource ID="SqlOrderDet" runat="server" ConnectionString="<%$ ConnectionStrings:beowulfConnectionString2 %>" SelectCommand="SELECT * FROM [View_EC_MAN_RA_Form] WHERE ([OrderId] = @OrderId)" InsertCommand="INSERT INTO AO_EC_MAN_RETN_STAGE(OrderId, OrderLine, style, style_name, Color, color_name, size, upc, qty_retn, Rsn_code, Rsn_Desc, Proc_Date, UserId, NO_Refund) VALUES (@OrderId, @OrderLine, @style, @style_name, @Color, @color_name, @size, @upc, @qty_retn, @Rsn_code, @Rsn_Desc, GETDATE(), @UserId, @NO_Refund)">
                            <InsertParameters>
                                <asp:Parameter Name="OrderId" />
                                <asp:Parameter Name="OrderLine" />
                                <asp:Parameter Name="style" />
                                <asp:Parameter Name="style_name" />
                                <asp:Parameter Name="Color" />
                                <asp:Parameter Name="color_name" />
                                <asp:Parameter Name="size" />
                                <asp:Parameter Name="upc" />
                                <asp:Parameter Name="qty_retn" />
                                <asp:Parameter Name="Rsn_code" />
                                <asp:Parameter Name="Rsn_Desc" />
                                <asp:Parameter Name="UserId" />
                                <asp:Parameter Name="NO_Refund" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:ControlParameter ControlID="TXT_OrderID" Name="OrderId" PropertyName="Text" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <asp:Image ID="Image1" runat="server" Height="52px" ImageUrl="~/Image/SkinnySeveralAsianlion.gif" Width="56px" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <div class="text-Center" style="width: 100%">
                               <asp:ListView ID="ListView2" runat="server" DataSourceID="SqlOrderDet">
                                   <AlternatingItemTemplate>
                                     <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: auto; font-size: 12pt; color: #616161; height: auto"><strong>Order Line</strong></td>
                                        <td style="width: auto; font-size: 12pt; color: #616161; height: auto"><strong>Style Details</strong></td>
                                         <td style="width: auto; height: auto"><strong><span style="font-size: 12pt; color: #616161">Return</span><br style="font-size: 12pt; color: #616161" /> <span style="font-size: 12pt; color: #616161">Reason</span></strong></td>
                                        <td style="width: auto; font-size: 12pt; color: #616161; height: auto"><strong>Qty.Ordered</strong></td>
                                        <td style="width: auto; font-size: 12pt; color: #616161; height: auto"><strong><span style="font-size: 12pt; color: #616161">Qty. To be</span><br style="font-size: 12pt; color: #616161" /> <span style="font-size: 12pt; color: #616161">Returned</span></strong></td>
                                        <td style="width: auto; font-size: 12pt; color: #616161; height: auto"><strong>Refund</strong></td>
                                       </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="OrderLineLabel" runat="server" Text='<%# Eval("OrderLine") %>' />
                                        </td>
                                        <td>STYLE:
                                            <asp:Label ID="styleLabel0" runat="server" Text='<%# Eval("style") %>'></asp:Label>
                                            <br />
                                            <asp:Label ID="style_nameLabel0" runat="server" Text='<%# Eval("style_name") %>'></asp:Label>
                                            <br />
                                            COLOR:
                                            <asp:Label ID="ColorLabel0" runat="server" Text='<%# Eval("Color") %>'></asp:Label>
                                            <br />
                                            <asp:Label ID="color_nameLabel0" runat="server" Text='<%# Eval("color_name") %>'></asp:Label>
                                            <br />
                                            SIZE:
                                            <asp:Label ID="sizeLabel0" runat="server" Text='<%# Eval("size") %>'></asp:Label>
                                            <br />
                                            UPC:
                                            <asp:Label ID="upcLabel0" runat="server" Text='<%# Eval("upc") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlReason" DataTextField="rsn_desc" DataValueField="rsn_code" Height="40px" Width="286px">
                                            </asp:DropDownList>
                                        </td>
                                         <td>
                                            <asp:Label ID="Qty_ordLabel0" runat="server" Text='<%# Eval("Qty_ord") %>'></asp:Label>
                                        </td>
                                        <td>
                                         <uc1:counter ID="UC1" runat="server" />
                                          </td>
                                        <td>
                                            <asp:CheckBox ID="CK_Refund" runat="server" Text=" Do Not Credit Customer" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                   
                                </table>
                                <br />
                                <br />
                                </td>
                                   </AlternatingItemTemplate>
                                   <EditItemTemplate>
                                       <span style="">OrderId:
                                       <asp:TextBox ID="OrderIdTextBox" runat="server" Text='<%# Bind("OrderId") %>' />
                                       <br />
                                       OrderLine:
                                       <asp:TextBox ID="OrderLineTextBox" runat="server" Text='<%# Bind("OrderLine") %>' />
                                       <br />
                                       style:
                                       <asp:TextBox ID="styleTextBox" runat="server" Text='<%# Bind("style") %>' />
                                       <br />
                                       style_name:
                                       <asp:TextBox ID="style_nameTextBox" runat="server" Text='<%# Bind("style_name") %>' />
                                       <br />
                                       Color:
                                       <asp:TextBox ID="ColorTextBox" runat="server" Text='<%# Bind("Color") %>' />
                                       <br />
                                       color_name:
                                       <asp:TextBox ID="color_nameTextBox" runat="server" Text='<%# Bind("color_name") %>' />
                                       <br />
                                       SIZE:
                                       <asp:TextBox ID="sizeTextBox" runat="server" Text='<%# Bind("size") %>' />
                                       <br />
                                       UPC:
                                       <asp:TextBox ID="upcTextBox" runat="server" Text='<%# Bind("upc") %>' />
                                       <br />
                                       Qty_ord:
                                       <asp:TextBox ID="Qty_ordTextBox" runat="server" Text='<%# Bind("Qty_ord") %>' />
                                       <br />
                                       <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Update" />
                                       <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" />
                                       <br />
                                       <br />
                                       </span>
                                   </EditItemTemplate>
                                   <EmptyDataTemplate>
                                       <span></span>
                                   </EmptyDataTemplate>
                                   <InsertItemTemplate>
                                       <span style="">OrderId:
                                       <asp:TextBox ID="OrderIdTextBox" runat="server" Text='<%# Bind("OrderId") %>' />
                                       <br />
                                       OrderLine:
                                       <asp:TextBox ID="OrderLineTextBox" runat="server" Text='<%# Bind("OrderLine") %>' />
                                       <br />
                                       style:
                                       <asp:TextBox ID="styleTextBox" runat="server" Text='<%# Bind("style") %>' />
                                       <br />
                                       style_name:
                                       <asp:TextBox ID="style_nameTextBox" runat="server" Text='<%# Bind("style_name") %>' />
                                       <br />
                                       Color:
                                       <asp:TextBox ID="ColorTextBox" runat="server" Text='<%# Bind("Color") %>' />
                                       <br />
                                       color_name:
                                       <asp:TextBox ID="color_nameTextBox" runat="server" Text='<%# Bind("color_name") %>' />
                                       <br />
                                       size:
                                       <asp:TextBox ID="sizeTextBox" runat="server" Text='<%# Bind("size") %>' />
                                       <br />
                                       upc:
                                       <asp:TextBox ID="upcTextBox" runat="server" Text='<%# Bind("upc") %>' />
                                       <br />
                                       Qty_ord:
                                       <asp:TextBox ID="Qty_ordTextBox" runat="server" Text='<%# Bind("Qty_ord") %>' />
                                       <br />
                                       <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="Insert" />
                                       <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Clear" />
                                       <br />
                                       <br />
                                       </span>
                                   </InsertItemTemplate>
                                   <ItemTemplate>
                                 <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: auto; font-size: 12pt; color: #616161; height: auto"><strong>Order Line</strong></td>
                                        <td style="width: auto; font-size: 12pt; color: #616161; height: auto"><strong>Style Details</strong></td>
                                         <td style="width: auto; height: auto"><strong><span style="font-size: 12pt; color: #616161">Return</span><br style="font-size: 12pt; color: #616161" /> <span style="font-size: 12pt; color: #616161">Reason</span></strong></td>
                                        <td style="width: auto; font-size: 12pt; color: #616161; height: auto"><strong>Qty.Ordered</strong></td>
                                        <td style="width: auto; font-size: 12pt; color: #616161; height: auto"><strong><span style="font-size: 12pt; color: #616161">Qty. To be</span><br style="font-size: 12pt; color: #616161" /> <span style="font-size: 12pt; color: #616161">Returned</span></strong></td>
                                        <td style="width: auto; font-size: 12pt; color: #616161; height: auto"><strong>Refund</strong></td>
                                       </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="OrderLineLabel" runat="server" Text='<%# Eval("OrderLine") %>' />
                                        </td>
                                        <td>STYLE:
                                            <asp:Label ID="styleLabel0" runat="server" Text='<%# Eval("style") %>'></asp:Label>
                                            <br />
                                            <asp:Label ID="style_nameLabel0" runat="server" Text='<%# Eval("style_name") %>'></asp:Label>
                                            <br />
                                            COLOR:
                                            <asp:Label ID="ColorLabel0" runat="server" Text='<%# Eval("Color") %>'></asp:Label>
                                            <br />
                                            <asp:Label ID="color_nameLabel0" runat="server" Text='<%# Eval("color_name") %>'></asp:Label>
                                            <br />
                                            SIZE:
                                            <asp:Label ID="sizeLabel0" runat="server" Text='<%# Eval("size") %>'></asp:Label>
                                            <br />
                                            UPC:
                                            <asp:Label ID="upcLabel0" runat="server" Text='<%# Eval("upc") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlReason" DataTextField="rsn_desc" DataValueField="rsn_code" Height="40px" Width="286px">
                                            </asp:DropDownList>
                                        </td>
                                         <td>
                                            <asp:Label ID="Qty_ordLabel0" runat="server" Text='<%# Eval("Qty_ord") %>'></asp:Label>
                                        </td>
                                        <td>
                                         <uc1:counter ID="UC1" runat="server"  />
                                          </td>
                                        <td>
                                            <asp:CheckBox ID="CK_Refund" runat="server" Text=" Do Not Credit Customer" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                   
                                </table>
                                <br />
                                <br />
                                </td>
                                   </ItemTemplate>
                                   <LayoutTemplate>
                                       <div id="itemPlaceholderContainer" runat="server" style="">
                                           <span runat="server" id="itemPlaceholder" />
                                       </div>
                                       <div style="">
                                       </div>
                                   </LayoutTemplate>
                                   <SelectedItemTemplate>
                                       <span style="">OrderId:
                                       <asp:Label ID="OrderIdLabel" runat="server" Text='<%# Eval("OrderId") %>' />
                                       <br />
                                       OrderLine:
                                       <asp:Label ID="OrderLineLabel" runat="server" Text='<%# Eval("OrderLine") %>' />
                                       <br />
                                       style:
                                       <asp:Label ID="styleLabel" runat="server" Text='<%# Eval("style") %>' />
                                       <br />
                                       style_name:
                                       <asp:Label ID="style_nameLabel" runat="server" Text='<%# Eval("style_name") %>' />
                                       <br />
                                       Color:
                                       <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("Color") %>' />
                                       <br />
                                       color_name:
                                       <asp:Label ID="color_nameLabel" runat="server" Text='<%# Eval("color_name") %>' />
                                       <br />
                                       size:
                                       <asp:Label ID="sizeLabel" runat="server" Text='<%# Eval("size") %>' />
                                       <br />
                                       upc:
                                       <asp:Label ID="upcLabel" runat="server" Text='<%# Eval("upc") %>' />
                                       <br />
                                       Qty_ord:
                                       <asp:Label ID="Qty_ordLabel" runat="server" Text='<%# Eval("Qty_ord") %>' />
                                       <br />
                                       <br />
                                       </span>
                                   </SelectedItemTemplate>
                               </asp:ListView>
                               <td runat="server" style="">
                                        <br />
                                   <br />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center">
                        <asp:Label ID="LBL_Total" runat="server" Font-Bold="True" ForeColor="#333333" style="font-size: x-large"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LBL_Error" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="text-right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-right">
                        <asp:Button ID="BTN_Process" runat="server" BackColor="#4D4F6F" BorderStyle="Solid" Font-Size="Small" ForeColor="White" Height="40px" Text="PROCESS" Width="140px" Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
