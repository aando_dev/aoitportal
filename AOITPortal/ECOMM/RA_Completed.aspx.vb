﻿Public Class RA_Completed
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("totalqnty") Is Nothing Then
            Response.Redirect("RA_Entry_Form.aspx")
        ElseIf CInt(Session("totalqnty") > 0) Then

            Session.Remove("totalqnty")
            Label1.Text = "RA process completed successfully!" + vbCrLf + " RA's will be created in BlueCherry within the next 2 hours"
        Else

            Session.Remove("totalqnty")
            Label1.Text = "No Item has been returned!"

        End If


    End Sub

    Protected Sub BTN_Process_Click(sender As Object, e As EventArgs) Handles BTN_Process.Click
        Response.Redirect("RA_Entry_Form.aspx")
    End Sub
End Class