﻿
Imports EasyPost
Public Class Ship
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ScreenId As Integer = 5
        Try
            Dim dapUA As New AOWebPortaDSTableAdapters.UserAccessTableAdapter
            Dim ds As New AOWebPortaDS.UserAccessDataTable
            dapUA.FillByUserAndScreen(ds, User.Identity.Name, ScreenId)
            If ds.Rows.Count = 0 Then
                Response.Redirect("../Error.aspx")
            Else
                Dim dapLog As New AOWebPortaDSTableAdapters.LoginLogsTableAdapter
                dapLog.InsertLogin(User.Identity.Name, ScreenId)
            End If
        Catch ex As Exception
            Response.Redirect("../Error.aspx")
        End Try
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Label1.Text = ""
        Dim ds As New DataSet1.View_NotShippedCartonsDataTable
        Dim dap As New DataSet1TableAdapters.View_NotShippedCartonsTableAdapter
        Dim insertdap As New DataSet1TableAdapters.ShippedCartonsTableAdapter
        dap.FillBycarton_num(ds, TXTCartonNum.Text)
        If ds.Rows.Count > 1 Then
            Try
                EasyPost.ClientManager.SetCurrent("EZTKc36f9b14b4ef40339bf77be709d242fbCsAbVfxik33uBkE8aW36Pg")
                Dim batchdata As New Dictionary(Of String, Object) From {{"reference", ds.First.pick_num.ToString}}
                Dim bachtid As String = EasyPost.Batch.Create(batchdata).id

                For Each ro As DataRow In ds.Rows
                    Dim to_address_det As New Dictionary(Of String, Object) From {
      {"name", ro.Item("sh_name").ToString},
      {"street1", ro.Item("sh_Addr1").ToString.Trim},
      {"street2", ro.Item("sh_Addr1").ToString},
      {"city", ro.Item("sh_city").ToString},
      {"state", ro.Item("sh_state").ToString},
      {"country", ro.Item("sh_country").ToString},
      {"phone", ro.Item("sh_phone").ToString},
      {"email", ro.Item("sh_email").ToString},
      {"zip", ro.Item("sh_zip").ToString}
    }
                    Dim from_address_det As New Dictionary(Of String, Object) From {
      {"name", ro.Item("ship_from_company").ToString},
      {"street1", ro.Item("Cust_Name").ToString.Trim},
      {"street2", ro.Item("ship_from_add1").ToString},
      {"city", ro.Item("ship_from_add2").ToString},
      {"state", ro.Item("ship_from_state").ToString},
      {"country", ro.Item("ship_from_country").ToString},
      {"phone", ro.Item("ship_from_phone").ToString},
      {"email", ro.Item("ship_from_email").ToString},
      {"zip", ro.Item("ship_from_zip").ToString}
    }
                    Dim return_address_det As New Dictionary(Of String, Object) From {
      {"name", ro.Item("retn_loc_name").ToString},
      {"street1", ro.Item("retn_loc_add1").ToString.Trim},
      {"street2", ro.Item("retn_loc_add2").ToString},
      {"city", ro.Item("retn_loc_city").ToString},
      {"state", ro.Item("retn_loc_state").ToString},
      {"country", ro.Item("retn_loc_country").ToString},
      {"zip", ro.Item("retn_loc_zip").ToString}
    }
                    Dim buyer_address_det As New Dictionary(Of String, Object) From {
      {"name", ro.Item("sh_name").ToString},
      {"street1", ro.Item("sh_Addr1").ToString.Trim},
      {"street2", ro.Item("sh_Addr1").ToString},
      {"city", ro.Item("sh_city").ToString},
      {"state", ro.Item("sh_state").ToString},
      {"country", ro.Item("sh_country").ToString},
      {"phone", ro.Item("sh_phone").ToString},
      {"email", ro.Item("sh_email").ToString},
      {"zip", ro.Item("sh_zip").ToString}
    }

                    Dim TPB As New Dictionary(Of String, Object) From {
      {"bill_third_party_postal_code", ro.Item("TPB_Zip_Code").ToString},
      {"bill_third_party_country", ro.Item("TPB_Country").ToString.Trim},
      {"bill_third_party_account", ro.Item("tpb_Acct").ToString}
    }
                    Dim parceldet As New Dictionary(Of String, Object) From {
      {"length", ro.Item("carton_len").ToString},
      {"width", ro.Item("carton_wdt").ToString.Trim},
      {"height", ro.Item("carton_hgt").ToString},
      {"weight", ro.Item("carton_wgt").ToString}
    }
                    Dim Ratedet As New Dictionary(Of String, Object) From {
      {"carrier", ro.Item("CARRIER").ToString}}

                    Dim shipmintid As String = Shipment.Create(New Dictionary(Of String, Object) From {{"reference", ro.Item("carton_num").ToString},
      {"to_address", to_address_det},
      {"from_address", from_address_det},
      {"return_address", return_address_det},
      {"buyer_address", buyer_address_det},
      {"parcel", parceldet},
      {"options", TPB}
     }).id 'Need to add carrier_accounts paramater for each shipment
                    insertdap.InsertShipment(ro.Item(0).ToString, ro.Item(1).ToString, ro.Item(2).ToString, ro.Item(3).ToString, ro.Item(4).ToString, ro.Item(5).ToString, ro.Item(6).ToString, ro.Item(7).ToString, ro.Item(8).ToString, ro.Item(9).ToString, ro.Item(10).ToString, ro.Item(11).ToString, ro.Item(12).ToString, ro.Item(13).ToString, ro.Item(14).ToString, ro.Item(15).ToString, ro.Item(16).ToString, ro.Item(17).ToString, ro.Item(18).ToString, ro.Item(19).ToString, ro.Item(20).ToString, ro.Item(21).ToString, ro.Item(22).ToString, ro.Item(23).ToString, ro.Item(24).ToString, ro.Item(25).ToString, ro.Item(26).ToString, ro.Item(27).ToString, ro.Item(28).ToString, ro.Item(29).ToString, ro.Item(30).ToString, ro.Item(31).ToString, ro.Item(32).ToString, ro.Item(33).ToString, ro.Item(34).ToString, ro.Item(35).ToString, ro.Item(36).ToString, ro.Item(37).ToString, ro.Item(38).ToString, ro.Item(39).ToString, ro.Item(40).ToString, ro.Item(41).ToString, ro.Item(42).ToString, ro.Item(43).ToString, ro.Item(44).ToString, ro.Item(45).ToString, ro.Item(46).ToString, ro.Item(47).ToString, ro.Item(48).ToString, ro.Item(49).ToString, ro.Item(50).ToString, ro.Item(51).ToString, ro.Item(52).ToString, ro.Item(53).ToString, ro.Item(54).ToString, ro.Item(55).ToString, ro.Item(56).ToString, ro.Item(57).ToString, ro.Item(58).ToString, ro.Item(59).ToString, ro.Item(60).ToString, ro.Item(61).ToString, ro.Item(62).ToString, ro.Item(63).ToString, ro.Item(64).ToString, ro.Item(65).ToString, ro.Item(66).ToString, ro.Item(67).ToString, ro.Item(68).ToString, ro.Item(69).ToString, ro.Item(70).ToString, ro.Item(71).ToString, ro.Item(72).ToString, ro.Item(73).ToString, ro.Item(74).ToString, ro.Item(75).ToString, ro.Item(76).ToString, ro.Item(77).ToString, ro.Item(78).ToString, ro.Item(79).ToString, ro.Item(80).ToString, ro.Item(81).ToString, ro.Item(82).ToString, ro.Item(83).ToString, ro.Item(84).ToString, My.Computer.Name, "Admin", shipmintid)
                    Shipment.Retrieve(shipmintid).Buy(Shipment.Retrieve(shipmintid).LowestRate)
                    Batch.Retrieve(bachtid).AddShipments(New List(Of Shipment) From {{Shipment.Retrieve(shipmintid)}})
                Next
                Label1.ForeColor = System.Drawing.Color.Green

                Label1.Text = "Shipment Successfully Created !"
                Try
                    Batch.Retrieve(bachtid).GenerateLabel("pdf")
                Catch ex As Exception

                End Try

                TXTCartonNum.Text = ""
                HyperLink1.NavigateUrl = Batch.Retrieve(bachtid).label_url
            Catch ex As Exception
                Label1.ForeColor = System.Drawing.Color.Red
                Label1.Text = ex.Message
            End Try
        ElseIf ds.Rows.Count = 1 Then
            Try
                EasyPost.ClientManager.SetCurrent("EZTKc36f9b14b4ef40339bf77be709d242fbCsAbVfxik33uBkE8aW36Pg")
                Dim shipmintid As String
                For Each ro As DataRow In ds.Rows
                    Dim to_address_det As New Dictionary(Of String, Object) From {
      {"name", ro.Item("sh_name").ToString},
      {"street1", ro.Item("sh_Addr1").ToString.Trim},
      {"street2", ro.Item("sh_Addr1").ToString},
      {"city", ro.Item("sh_city").ToString},
      {"state", ro.Item("sh_state").ToString},
      {"country", ro.Item("sh_country").ToString},
      {"phone", ro.Item("sh_phone").ToString},
      {"email", ro.Item("sh_email").ToString},
      {"zip", ro.Item("sh_zip").ToString}
    }
                    Dim from_address_det As New Dictionary(Of String, Object) From {
      {"name", ro.Item("ship_from_company").ToString},
      {"street1", ro.Item("Cust_Name").ToString.Trim},
      {"street2", ro.Item("ship_from_add1").ToString},
      {"city", ro.Item("ship_from_add2").ToString},
      {"state", ro.Item("ship_from_state").ToString},
      {"country", ro.Item("ship_from_country").ToString},
      {"phone", ro.Item("ship_from_phone").ToString},
      {"email", ro.Item("ship_from_email").ToString},
      {"zip", ro.Item("ship_from_zip").ToString}
    }
                    Dim return_address_det As New Dictionary(Of String, Object) From {
      {"name", ro.Item("retn_loc_name").ToString},
      {"street1", ro.Item("retn_loc_add1").ToString.Trim},
      {"street2", ro.Item("retn_loc_add2").ToString},
      {"city", ro.Item("retn_loc_city").ToString},
      {"state", ro.Item("retn_loc_state").ToString},
      {"country", ro.Item("retn_loc_country").ToString},
      {"zip", ro.Item("retn_loc_zip").ToString}
    }
                    Dim buyer_address_det As New Dictionary(Of String, Object) From {
      {"name", ro.Item("sh_name").ToString},
      {"street1", ro.Item("sh_Addr1").ToString.Trim},
      {"street2", ro.Item("sh_Addr1").ToString},
      {"city", ro.Item("sh_city").ToString},
      {"state", ro.Item("sh_state").ToString},
      {"country", ro.Item("sh_country").ToString},
      {"phone", ro.Item("sh_phone").ToString},
      {"email", ro.Item("sh_email").ToString},
      {"zip", ro.Item("sh_zip").ToString}
    }

                    Dim TPB As New Dictionary(Of String, Object) From {
      {"bill_third_party_postal_code", ro.Item("TPB_Zip_Code").ToString},
      {"bill_third_party_country", ro.Item("TPB_Country").ToString.Trim},
      {"bill_third_party_account", ro.Item("tpb_Acct").ToString}
    }
                    Dim parceldet As New Dictionary(Of String, Object) From {
      {"length", ro.Item("carton_len").ToString},
      {"width", ro.Item("carton_wdt").ToString.Trim},
      {"height", ro.Item("carton_hgt").ToString},
      {"weight", ro.Item("carton_wgt").ToString}
    }
                    Dim Ratedet As New Dictionary(Of String, Object) From {
      {"carrier", ro.Item("CARRIER").ToString}}

                    shipmintid = Shipment.Create(New Dictionary(Of String, Object) From {{"reference", ro.Item("carton_num").ToString},
      {"to_address", to_address_det},
      {"from_address", from_address_det},
      {"return_address", return_address_det},
      {"buyer_address", buyer_address_det},
      {"parcel", parceldet},
      {"options", TPB}
     }).id 'Need to add carrier_accounts paramater for each shipment
                    insertdap.InsertShipment(ro.Item(0).ToString, ro.Item(1).ToString, ro.Item(2).ToString, ro.Item(3).ToString, ro.Item(4).ToString, ro.Item(5).ToString, ro.Item(6).ToString, ro.Item(7).ToString, ro.Item(8).ToString, ro.Item(9).ToString, ro.Item(10).ToString, ro.Item(11).ToString, ro.Item(12).ToString, ro.Item(13).ToString, ro.Item(14).ToString, ro.Item(15).ToString, ro.Item(16).ToString, ro.Item(17).ToString, ro.Item(18).ToString, ro.Item(19).ToString, ro.Item(20).ToString, ro.Item(21).ToString, ro.Item(22).ToString, ro.Item(23).ToString, ro.Item(24).ToString, ro.Item(25).ToString, ro.Item(26).ToString, ro.Item(27).ToString, ro.Item(28).ToString, ro.Item(29).ToString, ro.Item(30).ToString, ro.Item(31).ToString, ro.Item(32).ToString, ro.Item(33).ToString, ro.Item(34).ToString, ro.Item(35).ToString, ro.Item(36).ToString, ro.Item(37).ToString, ro.Item(38).ToString, ro.Item(39).ToString, ro.Item(40).ToString, ro.Item(41).ToString, ro.Item(42).ToString, ro.Item(43).ToString, ro.Item(44).ToString, ro.Item(45).ToString, ro.Item(46).ToString, ro.Item(47).ToString, ro.Item(48).ToString, ro.Item(49).ToString, ro.Item(50).ToString, ro.Item(51).ToString, ro.Item(52).ToString, ro.Item(53).ToString, ro.Item(54).ToString, ro.Item(55).ToString, ro.Item(56).ToString, ro.Item(57).ToString, ro.Item(58).ToString, ro.Item(59).ToString, ro.Item(60).ToString, ro.Item(61).ToString, ro.Item(62).ToString, ro.Item(63).ToString, ro.Item(64).ToString, ro.Item(65).ToString, ro.Item(66).ToString, ro.Item(67).ToString, ro.Item(68).ToString, ro.Item(69).ToString, ro.Item(70).ToString, ro.Item(71).ToString, ro.Item(72).ToString, ro.Item(73).ToString, ro.Item(74).ToString, ro.Item(75).ToString, ro.Item(76).ToString, ro.Item(77).ToString, ro.Item(78).ToString, ro.Item(79).ToString, ro.Item(80).ToString, ro.Item(81).ToString, ro.Item(82).ToString, ro.Item(83).ToString, ro.Item(84).ToString, My.Computer.Name, "Admin", shipmintid)
                    Dim Srate As New Rate
                    Srate = Shipment.Retrieve(shipmintid).rates(0)
                    Shipment.Retrieve(shipmintid).Buy(Srate)

                Next

                Label1.ForeColor = System.Drawing.Color.Green
                Label1.Text = "Shipment Successfully Created !"
                Try
                    Shipment.Retrieve(shipmintid).GenerateLabel("pdf")
                    Dim labelpath As String = Shipment.Retrieve(shipmintid).postage_label.label_url
                    Dim lbldap As New DataSet1TableAdapters.ShipmentsLabelTableAdapter
                    lbldap.InsertLabel(shipmintid, labelpath, User.Identity.Name)
                    HyperLink1.NavigateUrl = labelpath
                Catch ex As Exception

                End Try

                TXTCartonNum.Focus()


            Catch ex As Exception
                Label1.ForeColor = System.Drawing.Color.Red
                Label1.Text = ex.Message
            End Try

        Else
            Label1.ForeColor = System.Drawing.Color.Red
            Label1.Text = "Carton already Shipped"
        End If

        TXTCartonNum.Focus()

    End Sub

    Protected Sub TXTCartonNum_TextChanged(sender As Object, e As EventArgs) Handles TXTCartonNum.TextChanged

    End Sub
End Class