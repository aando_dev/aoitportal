﻿<%@ Page Title="Home Page"Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true"  CodeBehind="Default.aspx.vb" Inherits="AOITPortal.Contact"%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Alice and Olivia IT Dept. Applications </h1>
        <p class="lead">This Portal developed By A+O IT departmet, for help and support Please send to <a href="mailto:helpdesk@aliceandolivia.com">helpdesk@aliceandolivia.com</a> .</p>
    </div>

    <div class="row">
        <div class="col-md-4" style="left: 0px; top: 0px; width: 100%">
            <table style="width: 100%" >
                <tr>
                    <td class="text-center" style="height: 118px; width: 313px;">
                        <asp:ImageButton ID="ImageButton1" runat="server" Height="116px" ImageUrl="~/Image/Finance-PNG-File.png" Width="119px" PostBackUrl="~/GlInt.aspx" />
                    </td>
                    <td class="text-center" style="height: 118px; width: 314px;">
                        <asp:ImageButton ID="ImageButton2" runat="server" Height="116px" ImageUrl="~/Image/download.png" Width="119px" />
                    </td>
                    <td class="text-center" style="height: 118px; width: 314px;">
                        <asp:ImageButton ID="ImageButton3" runat="server" Height="116px" ImageUrl="~/Image/LMS-1-300x167.png" Width="119px" />
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="width: 313px">
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/GlInt.aspx">Finance</asp:HyperLink>
                    </td>
                    <td class="text-center" style="width: 314px">
                        <asp:HyperLink ID="HyperLink2" runat="server">Retail</asp:HyperLink>
                    </td>
                    <td class="text-center" style="width: 314px">
                        <asp:HyperLink ID="HyperLink3" runat="server">IT Department</asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td style="height: 27px; width: 313px;" class="text-center">
                        <asp:ImageButton ID="ImageButton4" runat="server" Height="116px" ImageUrl="~/Image/ecomm.png" Width="119px" PostBackUrl="~/ECOMM/ReturnGiftCard.aspx" />
                    </td>
                    <td style="height: 27px; width: 314px;" class="text-center">
                        <asp:ImageButton ID="ImageButton5" runat="server" Height="116px" ImageUrl="~/Image/features-med_carriers-544x544.png" Width="119px" PostBackUrl="~/Shipping/Ship.aspx" />
                    </td>
                    <td style="height: 27px; width: 314px;" class="text-center">
                        <asp:ImageButton ID="ImageButton6" runat="server" Height="116px" ImageUrl="~/Image/shopping-cart-bag-shows-basket-checkout.jpg" Width="119px" PostBackUrl="~/SampleSale/SampleSales.aspx" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 313px" class="text-center">
                        <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/ECOMM/ReturnGiftCard.aspx">eCommerce</asp:HyperLink>
                    </td>
                    <td style="width: 314px" class="text-center">
                        <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/Shipping/Ship.aspx">Shipping</asp:HyperLink>
                    </td>
                    <td style="width: 314px" class="text-center">
                        <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/SampleSale/SampleSales.aspx">Sample Sales</asp:HyperLink>
                    </td>
                </tr>
            </table>
        </div>
    </div>

</asp:Content>
