﻿
Public Class SampleSales
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ScreenId As Integer = 6
        Try
            Dim dapUA As New AOWebPortaDSTableAdapters.UserAccessTableAdapter
            Dim ds As New AOWebPortaDS.UserAccessDataTable
            dapUA.FillByUserAndScreen(ds, User.Identity.Name, ScreenId)
            If ds.Rows.Count = 0 Then
                Response.Redirect("../Error.aspx")
            Else
                Dim dapLog As New AOWebPortaDSTableAdapters.LoginLogsTableAdapter
                dapLog.InsertLogin(User.Identity.Name, ScreenId)
            End If
        Catch ex As Exception
            Response.Redirect("../Error.aspx")
        End Try
        Try
            Session.Clear()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub BtnNew_Click(sender As Object, e As EventArgs) Handles BtnNew.Click
        Label1.Text = ""
        TXTTransID.Text = ""
        '  If Session("TransID") Is Nothing Then
        If trnasid = 0 Then
            Dim dap As New AOWebPortaDSTableAdapters.SampleSales_TransMasterTableAdapter
            Try
                ' Session.Add("TransID", dap.InsertNewTrans(TXTFN.Text, TXTLN.Text, "", "").ToString)
                trnasid = dap.InsertNewTrans(TXTFN.Text, TXTLN.Text, "", "")
                TXTTransID.Text = trnasid 'Session("TransID").ToString
                TXTUPC.Text = ""
                TXTUPC.Focus()
            Catch ex As Exception
                Label1.Text = ex.Message
            End Try
        Else
            Label1.Text = "Current Sale is Open, pLease click End Sale Before Start New one! "
        End If

    End Sub

    Protected Sub BtnScan_Click(sender As Object, e As EventArgs) Handles BtnScan.Click
        Try
            Label1.Text = ""
            Label2.Text = ""
            'If Not Session("TransID") Is Nothing Then
            If trnasid > 0 Then
                Dim dap As New AOWebPortaDSTableAdapters.SampleSales_TransDetailsTableAdapter
                ' dap.InsertNewItem(CInt(Session("TransID").ToString), TXTUPC.Text, CDec(TXTPrice.Text), "", "")
                '    dap.InsertNewItem(Session("TransID").ToString, TXTUPC.Text, CDec(TXTPrice.Text), User.Identity.Name, My.Computer.Name)
                dap.InsertNewItem(trnasid.ToString, TXTUPC.Text, CDec(TXTPrice.Text), User.Identity.Name, My.Computer.Name)

                GridView2.DataBind()

                Label1.Text = "Total Items Count:" + CStr(GridView2.Rows.Count)

                TXTUPC.Text = ""
                TXTUPC.Focus()
            Else
                Label1.Text = "Please Click New Sale to Start!"
            End If
        Catch ex As Exception
            Label1.Text = ex.Message
        End Try
        If GridView2.Rows.Count > 0 Then
            HyperLink1.Visible = True
        Else
            HyperLink1.Visible = False
        End If

    End Sub




    Private Sub GridView2_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GridView2.RowCommand
        Try
            If e.CommandName = "DeleteCart" Then
                Dim roindex As Integer = Convert.ToInt32(e.CommandArgument)
                Dim dap As New AOWebPortaDSTableAdapters.SampleSales_TransDetailsTableAdapter
                dap.UpdateRemoveItem(User.Identity.Name, My.Computer.Name, GridView2.Rows(roindex).Cells(0).Text)
                GridView2.DataBind()
                Label1.Text = "Total Items Count:" + CStr(GridView2.Rows.Count)

                TXTUPC.Text = ""
                TXTUPC.Focus()

            End If

        Catch ex As Exception
            Label1.Text = ex.Message
        End Try
    End Sub

    Protected Sub BtnNew0_Click(sender As Object, e As EventArgs) Handles BtnNew0.Click
        '  If Not Session("TransID") Is Nothing Then

        ' Session.Clear()
        If trnasid > 0 Then
            trnasid = 0
            'Response.Redirect("PrintSampleSaleRecipt.aspx", "_blank")
            '   Process.Start("./PrintSampleSaleRecipt.aspx")
            HyperLink1.Visible = False
            TXTFN.Text = ""
            TXTLN.Text = ""
            TXTTransID.Text = ""
            TXTUPC.Text = ""
            TXTFN.Focus()
            Label1.Text = ""
            Label2.Text = ""
        End If

    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If DropDownList1.SelectedValue > 0 Then
            If Session("TransID") Is Nothing Then
                Session.Add("TransID", DropDownList1.SelectedValue)
                Response.Redirect("PrintSampleSaleRecipt.aspx")
            Else
                Session.Clear()
                Session.Add("TransID", DropDownList1.SelectedValue)
                Response.Redirect("PrintSampleSaleRecipt.aspx")
            End If

        End If
    End Sub

    Protected Sub TXTUPC_TextChanged(sender As Object, e As EventArgs) Handles TXTUPC.TextChanged

    End Sub
End Class