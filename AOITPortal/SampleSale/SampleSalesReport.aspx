﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SampleSalesReport.aspx.vb" Inherits="AOITPortal.Sample_Sales_Report" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="nav-justified">
                <tr>
                    <td class="text-center" colspan="4" style="font-size: large">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center" colspan="4" style="font-size: large">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center" colspan="4" style="font-size: large"><strong>Sample Sale Report </strong></td>
                </tr>
                <tr>
                    <td>Select Style </td>
                    <td colspan="2">
                        <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="StyleDS" DataTextField="STYLE" DataValueField="STYLE" Height="22px" Width="400px">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="StyleDS" runat="server" ConnectionString="<%$ ConnectionStrings:AOWebPortalConnectionString %>" SelectCommand="SELECT DISTINCT [STYLE] FROM [View_SampleSale_TransactionsDetails]"></asp:SqlDataSource>
                    </td>
                    <td>
                        <asp:Button ID="Button1" runat="server" style="height: 26px" Text="Load By Style" Width="150px" />
                    </td>
                </tr>
                <tr>
                    <td style="height: 22px">Select Color</td>
                    <td colspan="2" style="height: 22px">
                        <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="ColorDS" DataTextField="COLOR_CODE" DataValueField="COLOR_CODE" Height="22px" Width="400px">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="ColorDS" runat="server" ConnectionString="<%$ ConnectionStrings:AOWebPortalConnectionString %>" SelectCommand="SELECT DISTINCT [COLOR_CODE] FROM [View_SampleSale_TransactionsDetails]"></asp:SqlDataSource>
                    </td>
                    <td style="height: 22px">
                        <asp:Button ID="Button2" runat="server" Text="Load By Color" Width="150px" />
                    </td>
                </tr>
                <tr>
                    <td>Select Size</td>
                    <td colspan="2">
                        <asp:DropDownList ID="DropDownList3" runat="server" DataSourceID="SizeDS" DataTextField="Size" DataValueField="Size" Height="22px" Width="400px">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SizeDS" runat="server" ConnectionString="<%$ ConnectionStrings:AOWebPortalConnectionString %>" SelectCommand="SELECT DISTINCT [Size] FROM [View_SampleSale_TransactionsDetails]"></asp:SqlDataSource>
                    </td>
                    <td>
                        <asp:Button ID="Button3" runat="server" Text="Load By Size" Width="150px" />
                    </td>
                </tr>
                <tr>
                    <td style="height: 22px" colspan="4">
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <asp:Image ID="Image2" runat="server" Height="52px" ImageUrl="~/Image/SkinnySeveralAsianlion.gif" Width="56px" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 26px">
                        <asp:Button ID="Button5" runat="server" Text="Load all Transactions" Width="150px" />
                    </td>
                    <td colspan="2" style="height: 26px"></td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 26px">&nbsp;</td>
                    <td colspan="2" style="height: 26px">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 26px">Sold Quantity:&nbsp;
                        <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="#000099" Text="0"></asp:Label>
                        <br />
                        Total Price :
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Italic="False" ForeColor="#000099" Text="0"></asp:Label>
                    </td>
                    <td colspan="2" style="height: 26px">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="4" style="height: 20px">
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:AOWebPortalConnectionString %>" SelectCommand="SELECT TransID, TransDate, EmpFN, EmpLN, email, phone, TrnsDetID, UPC, Price, CreatedDT, CreatedUser, CreatedPC, DeletedDT, DeletedUser, DeletedPC, IsDeleted, division, STYLE, style_name, COLOR_CODE, Size, color_name, Label, Wholesale_Price, STD_COST, Retail_Price, Image FROM View_SampleSale_TransactionsDetails WHERE TransID=0 and (IsDeleted =0) ORDER BY TrnsDetID DESC"></asp:SqlDataSource>
                        <br />
                        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="TransID" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" Width="100%">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            <Columns>
                                <asp:BoundField DataField="EmpFN" HeaderText="First Name" SortExpression="EmpFN" />
                                <asp:BoundField DataField="EmpLN" HeaderText="Last name" SortExpression="EmpLN" />
                                <asp:BoundField DataField="UPC" HeaderText="UPC" SortExpression="UPC" />
                                <asp:BoundField DataField="STYLE" HeaderText="Style" SortExpression="STYLE" />
                                <asp:BoundField DataField="style_name" HeaderText="Style Name" SortExpression="style_name" />
                                <asp:BoundField DataField="COLOR_CODE" HeaderText="Color Code" SortExpression="COLOR_CODE" />
                                <asp:BoundField DataField="color_name" HeaderText="Color" SortExpression="color_name" />
                                <asp:BoundField DataField="Size" HeaderText="Size" SortExpression="Size" />
                                <asp:TemplateField HeaderText="Image">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Eval("Image") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Image ID="Image1" runat="server" Height="100px" ImageUrl='<%# Eval("Image") %>' Width="100px" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EditRowStyle BackColor="#999999" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 20px">&nbsp;</td>
                    <td colspan="2" style="height: 20px">&nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
