﻿Public Class FF_Print_Docs
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ScreenID As Integer = 9
        Try
            Dim dapUA As New AOWebPortaDSTableAdapters.UserAccessTableAdapter
            Dim ds As New AOWebPortaDS.UserAccessDataTable
            dapUA.FillByUserAndScreen(ds, User.Identity.Name, ScreenID)
            If ds.Rows.Count = 0 Then
                Response.Redirect("../Error.aspx")
            Else
                Dim dapLog As New AOWebPortaDSTableAdapters.LoginLogsTableAdapter
                dapLog.InsertLogin(User.Identity.Name, ScreenID)
            End If
        Catch ex As Exception
            Response.Redirect("../Error.aspx")
        End Try
        Me.Form.DefaultButton = BTN_Search1.UniqueID
    End Sub

    Protected Sub BTN_Search_Click(sender As Object, e As EventArgs) Handles BTN_Search.Click
        Try
            TXT_OrderID.Text = ""
            SqlDataSource_FF_Pick_Doc.SelectCommand = "select PO_NUM, Pick_num, Replace((ISNULL(finallink, '')),'\\aando.local\public\IT Public\FarFetch\Docs\','~\FF_Docs\') as Link,(case when finallink =null then 'N/A' when finallink ='' then 'N/A' else convert(varchar,Pick_num) End) as hltext from AO_FF_PICK_DOCS where Shipped is null and Pick_num='" + TXT_PickTicket.Text + "'"
            GridView1.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub BTN_Search0_Click(sender As Object, e As EventArgs) Handles BTN_Search0.Click
        Try
            TXT_PickTicket.Text = ""
            SqlDataSource_FF_Pick_Doc.SelectCommand = "select PO_NUM, Pick_num, Replace((ISNULL(finallink, '')),'\\aando.local\public\IT Public\FarFetch\Docs\','~\FF_Docs\') as Link,(case when finallink =null then 'N/A' when finallink ='' then 'N/A' else convert(varchar,Pick_num) End) as hltext from AO_FF_PICK_DOCS where Shipped is null and PO_num='" + TXT_OrderID.Text + "'"
            GridView1.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub BTN_Search1_Click(sender As Object, e As EventArgs) Handles BTN_Search1.Click
        Try
            TXT_PickTicket.Text = ""
            TXT_OrderID.Text = ""
            SqlDataSource_FF_Pick_Doc.SelectCommand = "select PO_NUM, Pick_num, Replace((ISNULL(finallink, '')),'\\aando.local\public\IT Public\FarFetch\Docs\','~\FF_Docs\') as Link,(case when finallink =null then 'N/A' when finallink ='' then 'N/A' else convert(varchar,Pick_num) End) as hltext from AO_FF_PICK_DOCS where Shipped is null "
            GridView1.DataBind()
        Catch ex As Exception

        End Try
    End Sub
End Class